package com.zheng.upms.rpc.service.impl;

import com.zheng.common.annotation.BaseService;
import com.zheng.common.base.BaseServiceImpl;
import com.zheng.upms.dao.mapper.UpmsPartnerMapper;
import com.zheng.upms.dao.model.UpmsPartner;
import com.zheng.upms.dao.model.UpmsPartnerExample;
import com.zheng.upms.rpc.api.UpmsPartnerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @description: description:
 * <p>
 * @author: upuptop
 * <p>
 * @qq: 337081267
 * <p>
 * @CSDN: http://blog.csdn.net/pyfysf
 * <p>
 * @cnblogs: http://www.cnblogs.com/upuptop
 * <p>
 * @blog: http://wintp.top
 * <p>
 * @email: pyfysf@163.com
 * <p>
 * @time: 2018/12/2018/12/26
 * <p>
 */
@Service
@Transactional
@BaseService
public class UpmsPartnerServiceImpl extends BaseServiceImpl<UpmsPartnerMapper, UpmsPartner, UpmsPartnerExample> implements UpmsPartnerService {

    @Autowired
    private UpmsPartnerMapper mUpmsPartnerMapper;
}
