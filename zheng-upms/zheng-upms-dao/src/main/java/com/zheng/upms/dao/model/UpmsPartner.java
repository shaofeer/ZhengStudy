package com.zheng.upms.dao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * upms_partner
 * @author
 */
public class UpmsPartner implements Serializable {
    private String id;

    /**
     * 名称
     */
    private String title;

    /**
     * 省id
     */
    private String provinceId;

    /**
     * 市id
     */
    private String cityId;

    /**
     * 区id
     */
    private String countyId;

    /**
     * 省名称
     */
    private String provinceName;

    /**
     * 市名称
     */
    private String cityName;

    /**
     * 区名称
     */
    private String countyName;

    /**
     * 经度
     */
    private Double mapx;

    /**
     * 维度
     */
    private Double mapy;

    /**
     * 地址
     */
    private String address;

    /**
     * 是否删除
     */
    private String isDel;

    /**
     * 是否禁用
     */
    private String isDis;

    /**
     * 删除时间
     */
    private String delTime;

    /**
     * 状态
     */
    private String state;

    /**
     * 时间
     */
    private String createtime;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 图片
     */
    private String img;

    /**
     * 编号
     */
    private String code;

    /**
     * 联系人
     */
    private String linkman;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 服务区域
     */
    private String services;

    /**
     * 入驻时间
     */
    private String times;

    /**
     * EMAIL
     */
    private String email;

    /**
     * 类型 0  品牌 1水暖商家  2水暖施工
     */
    private String types;

    /**
     * 系统管理员id
     */
    private String sysUserId;

    /**
     * 代理商id
     */
    private String agentId;

    /**
     * qq
     */
    private String qq;

    /**
     * 网址
     */
    private String website;

    /**
     * 分类集合
     */
    private String cates;

    /**
     * 0 默认 1 推荐
     */
    private String isTop;

    /**
     * 二级筛选
     */
    private String catesSecond;

    private String targetId;

    /**
     * 0 默认 1 推荐
     */
    private String isRight;

    /**
     * 0 隐藏 1 显示
     */
    private String websiteIsHide;

    /**
     * 实物外观
     */
    private Integer swwg;

    /**
     * 服务质量
     */
    private Integer fwzl;

    /**
     * 商品质量
     */
    private Integer spzl;

    /**
     * 配送效率
     */
    private Integer psxl;

    private Date createDate;

    private String createBy;

    private Integer orderSort;

    private String tel;

    private String partnerId;

    private Integer sort;

    private String isAdv;

    /**
     * 0 默认 1 接受预约
     */
    private String isAppo;

    /**
     * 接收短信的手机号
     */
    private String smsMobile;

    /**
     * 分类id
     */
    private String fenleiId;

    /**
     * 1 是管理员
     */
    private String isAdmin;

    /**
     * 关注人数
     */
    private Integer followNum;

    private String sourceId;

    private String dadaShopId;

    private String isSynchro;

    private String isDada;

    private String dadaCityName;

    private String dadaPwd;

    /**
     * 是否营业 0:已闭店 1:已开店
     */
    private String isYingye;

    /**
     * 1:是外卖推荐 0：不推荐
     */
    private String isTakeoutTop;

    /**
     * 是否包邮：“0”包邮，“1”不包邮
     */
    private String isEfp;

    /**
     * pc网站推荐   1推    0不推
     */
    private String isPcTop;

    private String isBanner;

    /**
     * 更新时间
     */
    private Date updateDate;

    /**
     * 退货地址
     */
    private String refundAddress;

    /**
     * 退货联系人
     */
    private String refundLinkman;

    /**
     * 退货手机号
     */
    private String refundMobile;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public Double getMapx() {
        return mapx;
    }

    public void setMapx(Double mapx) {
        this.mapx = mapx;
    }

    public Double getMapy() {
        return mapy;
    }

    public void setMapy(Double mapy) {
        this.mapy = mapy;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }

    public String getIsDis() {
        return isDis;
    }

    public void setIsDis(String isDis) {
        this.isDis = isDis;
    }

    public String getDelTime() {
        return delTime;
    }

    public void setDelTime(String delTime) {
        this.delTime = delTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(String sysUserId) {
        this.sysUserId = sysUserId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCates() {
        return cates;
    }

    public void setCates(String cates) {
        this.cates = cates;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    public String getCatesSecond() {
        return catesSecond;
    }

    public void setCatesSecond(String catesSecond) {
        this.catesSecond = catesSecond;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getIsRight() {
        return isRight;
    }

    public void setIsRight(String isRight) {
        this.isRight = isRight;
    }

    public String getWebsiteIsHide() {
        return websiteIsHide;
    }

    public void setWebsiteIsHide(String websiteIsHide) {
        this.websiteIsHide = websiteIsHide;
    }

    public Integer getSwwg() {
        return swwg;
    }

    public void setSwwg(Integer swwg) {
        this.swwg = swwg;
    }

    public Integer getFwzl() {
        return fwzl;
    }

    public void setFwzl(Integer fwzl) {
        this.fwzl = fwzl;
    }

    public Integer getSpzl() {
        return spzl;
    }

    public void setSpzl(Integer spzl) {
        this.spzl = spzl;
    }

    public Integer getPsxl() {
        return psxl;
    }

    public void setPsxl(Integer psxl) {
        this.psxl = psxl;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Integer getOrderSort() {
        return orderSort;
    }

    public void setOrderSort(Integer orderSort) {
        this.orderSort = orderSort;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIsAdv() {
        return isAdv;
    }

    public void setIsAdv(String isAdv) {
        this.isAdv = isAdv;
    }

    public String getIsAppo() {
        return isAppo;
    }

    public void setIsAppo(String isAppo) {
        this.isAppo = isAppo;
    }

    public String getSmsMobile() {
        return smsMobile;
    }

    public void setSmsMobile(String smsMobile) {
        this.smsMobile = smsMobile;
    }

    public String getFenleiId() {
        return fenleiId;
    }

    public void setFenleiId(String fenleiId) {
        this.fenleiId = fenleiId;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Integer getFollowNum() {
        return followNum;
    }

    public void setFollowNum(Integer followNum) {
        this.followNum = followNum;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getDadaShopId() {
        return dadaShopId;
    }

    public void setDadaShopId(String dadaShopId) {
        this.dadaShopId = dadaShopId;
    }

    public String getIsSynchro() {
        return isSynchro;
    }

    public void setIsSynchro(String isSynchro) {
        this.isSynchro = isSynchro;
    }

    public String getIsDada() {
        return isDada;
    }

    public void setIsDada(String isDada) {
        this.isDada = isDada;
    }

    public String getDadaCityName() {
        return dadaCityName;
    }

    public void setDadaCityName(String dadaCityName) {
        this.dadaCityName = dadaCityName;
    }

    public String getDadaPwd() {
        return dadaPwd;
    }

    public void setDadaPwd(String dadaPwd) {
        this.dadaPwd = dadaPwd;
    }

    public String getIsYingye() {
        return isYingye;
    }

    public void setIsYingye(String isYingye) {
        this.isYingye = isYingye;
    }

    public String getIsTakeoutTop() {
        return isTakeoutTop;
    }

    public void setIsTakeoutTop(String isTakeoutTop) {
        this.isTakeoutTop = isTakeoutTop;
    }

    public String getIsEfp() {
        return isEfp;
    }

    public void setIsEfp(String isEfp) {
        this.isEfp = isEfp;
    }

    public String getIsPcTop() {
        return isPcTop;
    }

    public void setIsPcTop(String isPcTop) {
        this.isPcTop = isPcTop;
    }

    public String getIsBanner() {
        return isBanner;
    }

    public void setIsBanner(String isBanner) {
        this.isBanner = isBanner;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRefundAddress() {
        return refundAddress;
    }

    public void setRefundAddress(String refundAddress) {
        this.refundAddress = refundAddress;
    }

    public String getRefundLinkman() {
        return refundLinkman;
    }

    public void setRefundLinkman(String refundLinkman) {
        this.refundLinkman = refundLinkman;
    }

    public String getRefundMobile() {
        return refundMobile;
    }

    public void setRefundMobile(String refundMobile) {
        this.refundMobile = refundMobile;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UpmsPartner other = (UpmsPartner) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getProvinceId() == null ? other.getProvinceId() == null : this.getProvinceId().equals(other.getProvinceId()))
            && (this.getCityId() == null ? other.getCityId() == null : this.getCityId().equals(other.getCityId()))
            && (this.getCountyId() == null ? other.getCountyId() == null : this.getCountyId().equals(other.getCountyId()))
            && (this.getProvinceName() == null ? other.getProvinceName() == null : this.getProvinceName().equals(other.getProvinceName()))
            && (this.getCityName() == null ? other.getCityName() == null : this.getCityName().equals(other.getCityName()))
            && (this.getCountyName() == null ? other.getCountyName() == null : this.getCountyName().equals(other.getCountyName()))
            && (this.getMapx() == null ? other.getMapx() == null : this.getMapx().equals(other.getMapx()))
            && (this.getMapy() == null ? other.getMapy() == null : this.getMapy().equals(other.getMapy()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getIsDel() == null ? other.getIsDel() == null : this.getIsDel().equals(other.getIsDel()))
            && (this.getIsDis() == null ? other.getIsDis() == null : this.getIsDis().equals(other.getIsDis()))
            && (this.getDelTime() == null ? other.getDelTime() == null : this.getDelTime().equals(other.getDelTime()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getImg() == null ? other.getImg() == null : this.getImg().equals(other.getImg()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getLinkman() == null ? other.getLinkman() == null : this.getLinkman().equals(other.getLinkman()))
            && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()))
            && (this.getServices() == null ? other.getServices() == null : this.getServices().equals(other.getServices()))
            && (this.getTimes() == null ? other.getTimes() == null : this.getTimes().equals(other.getTimes()))
            && (this.getEmail() == null ? other.getEmail() == null : this.getEmail().equals(other.getEmail()))
            && (this.getTypes() == null ? other.getTypes() == null : this.getTypes().equals(other.getTypes()))
            && (this.getSysUserId() == null ? other.getSysUserId() == null : this.getSysUserId().equals(other.getSysUserId()))
            && (this.getAgentId() == null ? other.getAgentId() == null : this.getAgentId().equals(other.getAgentId()))
            && (this.getQq() == null ? other.getQq() == null : this.getQq().equals(other.getQq()))
            && (this.getWebsite() == null ? other.getWebsite() == null : this.getWebsite().equals(other.getWebsite()))
            && (this.getCates() == null ? other.getCates() == null : this.getCates().equals(other.getCates()))
            && (this.getIsTop() == null ? other.getIsTop() == null : this.getIsTop().equals(other.getIsTop()))
            && (this.getCatesSecond() == null ? other.getCatesSecond() == null : this.getCatesSecond().equals(other.getCatesSecond()))
            && (this.getTargetId() == null ? other.getTargetId() == null : this.getTargetId().equals(other.getTargetId()))
            && (this.getIsRight() == null ? other.getIsRight() == null : this.getIsRight().equals(other.getIsRight()))
            && (this.getWebsiteIsHide() == null ? other.getWebsiteIsHide() == null : this.getWebsiteIsHide().equals(other.getWebsiteIsHide()))
            && (this.getSwwg() == null ? other.getSwwg() == null : this.getSwwg().equals(other.getSwwg()))
            && (this.getFwzl() == null ? other.getFwzl() == null : this.getFwzl().equals(other.getFwzl()))
            && (this.getSpzl() == null ? other.getSpzl() == null : this.getSpzl().equals(other.getSpzl()))
            && (this.getPsxl() == null ? other.getPsxl() == null : this.getPsxl().equals(other.getPsxl()))
            && (this.getCreateDate() == null ? other.getCreateDate() == null : this.getCreateDate().equals(other.getCreateDate()))
            && (this.getCreateBy() == null ? other.getCreateBy() == null : this.getCreateBy().equals(other.getCreateBy()))
            && (this.getOrderSort() == null ? other.getOrderSort() == null : this.getOrderSort().equals(other.getOrderSort()))
            && (this.getTel() == null ? other.getTel() == null : this.getTel().equals(other.getTel()))
            && (this.getPartnerId() == null ? other.getPartnerId() == null : this.getPartnerId().equals(other.getPartnerId()))
            && (this.getSort() == null ? other.getSort() == null : this.getSort().equals(other.getSort()))
            && (this.getIsAdv() == null ? other.getIsAdv() == null : this.getIsAdv().equals(other.getIsAdv()))
            && (this.getIsAppo() == null ? other.getIsAppo() == null : this.getIsAppo().equals(other.getIsAppo()))
            && (this.getSmsMobile() == null ? other.getSmsMobile() == null : this.getSmsMobile().equals(other.getSmsMobile()))
            && (this.getFenleiId() == null ? other.getFenleiId() == null : this.getFenleiId().equals(other.getFenleiId()))
            && (this.getIsAdmin() == null ? other.getIsAdmin() == null : this.getIsAdmin().equals(other.getIsAdmin()))
            && (this.getFollowNum() == null ? other.getFollowNum() == null : this.getFollowNum().equals(other.getFollowNum()))
            && (this.getSourceId() == null ? other.getSourceId() == null : this.getSourceId().equals(other.getSourceId()))
            && (this.getDadaShopId() == null ? other.getDadaShopId() == null : this.getDadaShopId().equals(other.getDadaShopId()))
            && (this.getIsSynchro() == null ? other.getIsSynchro() == null : this.getIsSynchro().equals(other.getIsSynchro()))
            && (this.getIsDada() == null ? other.getIsDada() == null : this.getIsDada().equals(other.getIsDada()))
            && (this.getDadaCityName() == null ? other.getDadaCityName() == null : this.getDadaCityName().equals(other.getDadaCityName()))
            && (this.getDadaPwd() == null ? other.getDadaPwd() == null : this.getDadaPwd().equals(other.getDadaPwd()))
            && (this.getIsYingye() == null ? other.getIsYingye() == null : this.getIsYingye().equals(other.getIsYingye()))
            && (this.getIsTakeoutTop() == null ? other.getIsTakeoutTop() == null : this.getIsTakeoutTop().equals(other.getIsTakeoutTop()))
            && (this.getIsEfp() == null ? other.getIsEfp() == null : this.getIsEfp().equals(other.getIsEfp()))
            && (this.getIsPcTop() == null ? other.getIsPcTop() == null : this.getIsPcTop().equals(other.getIsPcTop()))
            && (this.getIsBanner() == null ? other.getIsBanner() == null : this.getIsBanner().equals(other.getIsBanner()))
            && (this.getUpdateDate() == null ? other.getUpdateDate() == null : this.getUpdateDate().equals(other.getUpdateDate()))
            && (this.getRefundAddress() == null ? other.getRefundAddress() == null : this.getRefundAddress().equals(other.getRefundAddress()))
            && (this.getRefundLinkman() == null ? other.getRefundLinkman() == null : this.getRefundLinkman().equals(other.getRefundLinkman()))
            && (this.getRefundMobile() == null ? other.getRefundMobile() == null : this.getRefundMobile().equals(other.getRefundMobile()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getProvinceId() == null) ? 0 : getProvinceId().hashCode());
        result = prime * result + ((getCityId() == null) ? 0 : getCityId().hashCode());
        result = prime * result + ((getCountyId() == null) ? 0 : getCountyId().hashCode());
        result = prime * result + ((getProvinceName() == null) ? 0 : getProvinceName().hashCode());
        result = prime * result + ((getCityName() == null) ? 0 : getCityName().hashCode());
        result = prime * result + ((getCountyName() == null) ? 0 : getCountyName().hashCode());
        result = prime * result + ((getMapx() == null) ? 0 : getMapx().hashCode());
        result = prime * result + ((getMapy() == null) ? 0 : getMapy().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getIsDel() == null) ? 0 : getIsDel().hashCode());
        result = prime * result + ((getIsDis() == null) ? 0 : getIsDis().hashCode());
        result = prime * result + ((getDelTime() == null) ? 0 : getDelTime().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getImg() == null) ? 0 : getImg().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getLinkman() == null) ? 0 : getLinkman().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        result = prime * result + ((getServices() == null) ? 0 : getServices().hashCode());
        result = prime * result + ((getTimes() == null) ? 0 : getTimes().hashCode());
        result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
        result = prime * result + ((getTypes() == null) ? 0 : getTypes().hashCode());
        result = prime * result + ((getSysUserId() == null) ? 0 : getSysUserId().hashCode());
        result = prime * result + ((getAgentId() == null) ? 0 : getAgentId().hashCode());
        result = prime * result + ((getQq() == null) ? 0 : getQq().hashCode());
        result = prime * result + ((getWebsite() == null) ? 0 : getWebsite().hashCode());
        result = prime * result + ((getCates() == null) ? 0 : getCates().hashCode());
        result = prime * result + ((getIsTop() == null) ? 0 : getIsTop().hashCode());
        result = prime * result + ((getCatesSecond() == null) ? 0 : getCatesSecond().hashCode());
        result = prime * result + ((getTargetId() == null) ? 0 : getTargetId().hashCode());
        result = prime * result + ((getIsRight() == null) ? 0 : getIsRight().hashCode());
        result = prime * result + ((getWebsiteIsHide() == null) ? 0 : getWebsiteIsHide().hashCode());
        result = prime * result + ((getSwwg() == null) ? 0 : getSwwg().hashCode());
        result = prime * result + ((getFwzl() == null) ? 0 : getFwzl().hashCode());
        result = prime * result + ((getSpzl() == null) ? 0 : getSpzl().hashCode());
        result = prime * result + ((getPsxl() == null) ? 0 : getPsxl().hashCode());
        result = prime * result + ((getCreateDate() == null) ? 0 : getCreateDate().hashCode());
        result = prime * result + ((getCreateBy() == null) ? 0 : getCreateBy().hashCode());
        result = prime * result + ((getOrderSort() == null) ? 0 : getOrderSort().hashCode());
        result = prime * result + ((getTel() == null) ? 0 : getTel().hashCode());
        result = prime * result + ((getPartnerId() == null) ? 0 : getPartnerId().hashCode());
        result = prime * result + ((getSort() == null) ? 0 : getSort().hashCode());
        result = prime * result + ((getIsAdv() == null) ? 0 : getIsAdv().hashCode());
        result = prime * result + ((getIsAppo() == null) ? 0 : getIsAppo().hashCode());
        result = prime * result + ((getSmsMobile() == null) ? 0 : getSmsMobile().hashCode());
        result = prime * result + ((getFenleiId() == null) ? 0 : getFenleiId().hashCode());
        result = prime * result + ((getIsAdmin() == null) ? 0 : getIsAdmin().hashCode());
        result = prime * result + ((getFollowNum() == null) ? 0 : getFollowNum().hashCode());
        result = prime * result + ((getSourceId() == null) ? 0 : getSourceId().hashCode());
        result = prime * result + ((getDadaShopId() == null) ? 0 : getDadaShopId().hashCode());
        result = prime * result + ((getIsSynchro() == null) ? 0 : getIsSynchro().hashCode());
        result = prime * result + ((getIsDada() == null) ? 0 : getIsDada().hashCode());
        result = prime * result + ((getDadaCityName() == null) ? 0 : getDadaCityName().hashCode());
        result = prime * result + ((getDadaPwd() == null) ? 0 : getDadaPwd().hashCode());
        result = prime * result + ((getIsYingye() == null) ? 0 : getIsYingye().hashCode());
        result = prime * result + ((getIsTakeoutTop() == null) ? 0 : getIsTakeoutTop().hashCode());
        result = prime * result + ((getIsEfp() == null) ? 0 : getIsEfp().hashCode());
        result = prime * result + ((getIsPcTop() == null) ? 0 : getIsPcTop().hashCode());
        result = prime * result + ((getIsBanner() == null) ? 0 : getIsBanner().hashCode());
        result = prime * result + ((getUpdateDate() == null) ? 0 : getUpdateDate().hashCode());
        result = prime * result + ((getRefundAddress() == null) ? 0 : getRefundAddress().hashCode());
        result = prime * result + ((getRefundLinkman() == null) ? 0 : getRefundLinkman().hashCode());
        result = prime * result + ((getRefundMobile() == null) ? 0 : getRefundMobile().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", title=").append(title);
        sb.append(", provinceId=").append(provinceId);
        sb.append(", cityId=").append(cityId);
        sb.append(", countyId=").append(countyId);
        sb.append(", provinceName=").append(provinceName);
        sb.append(", cityName=").append(cityName);
        sb.append(", countyName=").append(countyName);
        sb.append(", mapx=").append(mapx);
        sb.append(", mapy=").append(mapy);
        sb.append(", address=").append(address);
        sb.append(", isDel=").append(isDel);
        sb.append(", isDis=").append(isDis);
        sb.append(", delTime=").append(delTime);
        sb.append(", state=").append(state);
        sb.append(", createtime=").append(createtime);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", img=").append(img);
        sb.append(", code=").append(code);
        sb.append(", linkman=").append(linkman);
        sb.append(", mobile=").append(mobile);
        sb.append(", services=").append(services);
        sb.append(", times=").append(times);
        sb.append(", email=").append(email);
        sb.append(", types=").append(types);
        sb.append(", sysUserId=").append(sysUserId);
        sb.append(", agentId=").append(agentId);
        sb.append(", qq=").append(qq);
        sb.append(", website=").append(website);
        sb.append(", cates=").append(cates);
        sb.append(", isTop=").append(isTop);
        sb.append(", catesSecond=").append(catesSecond);
        sb.append(", targetId=").append(targetId);
        sb.append(", isRight=").append(isRight);
        sb.append(", websiteIsHide=").append(websiteIsHide);
        sb.append(", swwg=").append(swwg);
        sb.append(", fwzl=").append(fwzl);
        sb.append(", spzl=").append(spzl);
        sb.append(", psxl=").append(psxl);
        sb.append(", createDate=").append(createDate);
        sb.append(", createBy=").append(createBy);
        sb.append(", orderSort=").append(orderSort);
        sb.append(", tel=").append(tel);
        sb.append(", partnerId=").append(partnerId);
        sb.append(", sort=").append(sort);
        sb.append(", isAdv=").append(isAdv);
        sb.append(", isAppo=").append(isAppo);
        sb.append(", smsMobile=").append(smsMobile);
        sb.append(", fenleiId=").append(fenleiId);
        sb.append(", isAdmin=").append(isAdmin);
        sb.append(", followNum=").append(followNum);
        sb.append(", sourceId=").append(sourceId);
        sb.append(", dadaShopId=").append(dadaShopId);
        sb.append(", isSynchro=").append(isSynchro);
        sb.append(", isDada=").append(isDada);
        sb.append(", dadaCityName=").append(dadaCityName);
        sb.append(", dadaPwd=").append(dadaPwd);
        sb.append(", isYingye=").append(isYingye);
        sb.append(", isTakeoutTop=").append(isTakeoutTop);
        sb.append(", isEfp=").append(isEfp);
        sb.append(", isPcTop=").append(isPcTop);
        sb.append(", isBanner=").append(isBanner);
        sb.append(", updateDate=").append(updateDate);
        sb.append(", refundAddress=").append(refundAddress);
        sb.append(", refundLinkman=").append(refundLinkman);
        sb.append(", refundMobile=").append(refundMobile);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
