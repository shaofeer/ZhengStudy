package com.zheng.upms.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UpmsPartnerExample implements Serializable{
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public UpmsPartnerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("TITLE is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("TITLE is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("TITLE =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("TITLE <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("TITLE >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("TITLE >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("TITLE <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("TITLE <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("TITLE like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("TITLE not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("TITLE in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("TITLE not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("TITLE between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("TITLE not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIsNull() {
            addCriterion("PROVINCE_ID is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIsNotNull() {
            addCriterion("PROVINCE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceIdEqualTo(String value) {
            addCriterion("PROVINCE_ID =", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotEqualTo(String value) {
            addCriterion("PROVINCE_ID <>", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdGreaterThan(String value) {
            addCriterion("PROVINCE_ID >", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdGreaterThanOrEqualTo(String value) {
            addCriterion("PROVINCE_ID >=", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLessThan(String value) {
            addCriterion("PROVINCE_ID <", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLessThanOrEqualTo(String value) {
            addCriterion("PROVINCE_ID <=", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLike(String value) {
            addCriterion("PROVINCE_ID like", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotLike(String value) {
            addCriterion("PROVINCE_ID not like", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIn(List<String> values) {
            addCriterion("PROVINCE_ID in", values, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotIn(List<String> values) {
            addCriterion("PROVINCE_ID not in", values, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdBetween(String value1, String value2) {
            addCriterion("PROVINCE_ID between", value1, value2, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotBetween(String value1, String value2) {
            addCriterion("PROVINCE_ID not between", value1, value2, "provinceId");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("CITY_ID is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("CITY_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(String value) {
            addCriterion("CITY_ID =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(String value) {
            addCriterion("CITY_ID <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(String value) {
            addCriterion("CITY_ID >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(String value) {
            addCriterion("CITY_ID >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(String value) {
            addCriterion("CITY_ID <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(String value) {
            addCriterion("CITY_ID <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLike(String value) {
            addCriterion("CITY_ID like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotLike(String value) {
            addCriterion("CITY_ID not like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<String> values) {
            addCriterion("CITY_ID in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<String> values) {
            addCriterion("CITY_ID not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(String value1, String value2) {
            addCriterion("CITY_ID between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(String value1, String value2) {
            addCriterion("CITY_ID not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNull() {
            addCriterion("COUNTY_ID is null");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNotNull() {
            addCriterion("COUNTY_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCountyIdEqualTo(String value) {
            addCriterion("COUNTY_ID =", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotEqualTo(String value) {
            addCriterion("COUNTY_ID <>", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThan(String value) {
            addCriterion("COUNTY_ID >", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThanOrEqualTo(String value) {
            addCriterion("COUNTY_ID >=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThan(String value) {
            addCriterion("COUNTY_ID <", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThanOrEqualTo(String value) {
            addCriterion("COUNTY_ID <=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLike(String value) {
            addCriterion("COUNTY_ID like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotLike(String value) {
            addCriterion("COUNTY_ID not like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdIn(List<String> values) {
            addCriterion("COUNTY_ID in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotIn(List<String> values) {
            addCriterion("COUNTY_ID not in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdBetween(String value1, String value2) {
            addCriterion("COUNTY_ID between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotBetween(String value1, String value2) {
            addCriterion("COUNTY_ID not between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andProvinceNameIsNull() {
            addCriterion("PROVINCE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andProvinceNameIsNotNull() {
            addCriterion("PROVINCE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceNameEqualTo(String value) {
            addCriterion("PROVINCE_NAME =", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotEqualTo(String value) {
            addCriterion("PROVINCE_NAME <>", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameGreaterThan(String value) {
            addCriterion("PROVINCE_NAME >", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameGreaterThanOrEqualTo(String value) {
            addCriterion("PROVINCE_NAME >=", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameLessThan(String value) {
            addCriterion("PROVINCE_NAME <", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameLessThanOrEqualTo(String value) {
            addCriterion("PROVINCE_NAME <=", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameLike(String value) {
            addCriterion("PROVINCE_NAME like", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotLike(String value) {
            addCriterion("PROVINCE_NAME not like", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameIn(List<String> values) {
            addCriterion("PROVINCE_NAME in", values, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotIn(List<String> values) {
            addCriterion("PROVINCE_NAME not in", values, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameBetween(String value1, String value2) {
            addCriterion("PROVINCE_NAME between", value1, value2, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotBetween(String value1, String value2) {
            addCriterion("PROVINCE_NAME not between", value1, value2, "provinceName");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNull() {
            addCriterion("CITY_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNotNull() {
            addCriterion("CITY_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCityNameEqualTo(String value) {
            addCriterion("CITY_NAME =", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotEqualTo(String value) {
            addCriterion("CITY_NAME <>", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThan(String value) {
            addCriterion("CITY_NAME >", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("CITY_NAME >=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThan(String value) {
            addCriterion("CITY_NAME <", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThanOrEqualTo(String value) {
            addCriterion("CITY_NAME <=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLike(String value) {
            addCriterion("CITY_NAME like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotLike(String value) {
            addCriterion("CITY_NAME not like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameIn(List<String> values) {
            addCriterion("CITY_NAME in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotIn(List<String> values) {
            addCriterion("CITY_NAME not in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameBetween(String value1, String value2) {
            addCriterion("CITY_NAME between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotBetween(String value1, String value2) {
            addCriterion("CITY_NAME not between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNull() {
            addCriterion("COUNTY_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNotNull() {
            addCriterion("COUNTY_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCountyNameEqualTo(String value) {
            addCriterion("COUNTY_NAME =", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotEqualTo(String value) {
            addCriterion("COUNTY_NAME <>", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThan(String value) {
            addCriterion("COUNTY_NAME >", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThanOrEqualTo(String value) {
            addCriterion("COUNTY_NAME >=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThan(String value) {
            addCriterion("COUNTY_NAME <", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThanOrEqualTo(String value) {
            addCriterion("COUNTY_NAME <=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLike(String value) {
            addCriterion("COUNTY_NAME like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotLike(String value) {
            addCriterion("COUNTY_NAME not like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameIn(List<String> values) {
            addCriterion("COUNTY_NAME in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotIn(List<String> values) {
            addCriterion("COUNTY_NAME not in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameBetween(String value1, String value2) {
            addCriterion("COUNTY_NAME between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotBetween(String value1, String value2) {
            addCriterion("COUNTY_NAME not between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andMapxIsNull() {
            addCriterion("MAPX is null");
            return (Criteria) this;
        }

        public Criteria andMapxIsNotNull() {
            addCriterion("MAPX is not null");
            return (Criteria) this;
        }

        public Criteria andMapxEqualTo(Double value) {
            addCriterion("MAPX =", value, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxNotEqualTo(Double value) {
            addCriterion("MAPX <>", value, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxGreaterThan(Double value) {
            addCriterion("MAPX >", value, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxGreaterThanOrEqualTo(Double value) {
            addCriterion("MAPX >=", value, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxLessThan(Double value) {
            addCriterion("MAPX <", value, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxLessThanOrEqualTo(Double value) {
            addCriterion("MAPX <=", value, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxIn(List<Double> values) {
            addCriterion("MAPX in", values, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxNotIn(List<Double> values) {
            addCriterion("MAPX not in", values, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxBetween(Double value1, Double value2) {
            addCriterion("MAPX between", value1, value2, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapxNotBetween(Double value1, Double value2) {
            addCriterion("MAPX not between", value1, value2, "mapx");
            return (Criteria) this;
        }

        public Criteria andMapyIsNull() {
            addCriterion("MAPY is null");
            return (Criteria) this;
        }

        public Criteria andMapyIsNotNull() {
            addCriterion("MAPY is not null");
            return (Criteria) this;
        }

        public Criteria andMapyEqualTo(Double value) {
            addCriterion("MAPY =", value, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyNotEqualTo(Double value) {
            addCriterion("MAPY <>", value, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyGreaterThan(Double value) {
            addCriterion("MAPY >", value, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyGreaterThanOrEqualTo(Double value) {
            addCriterion("MAPY >=", value, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyLessThan(Double value) {
            addCriterion("MAPY <", value, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyLessThanOrEqualTo(Double value) {
            addCriterion("MAPY <=", value, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyIn(List<Double> values) {
            addCriterion("MAPY in", values, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyNotIn(List<Double> values) {
            addCriterion("MAPY not in", values, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyBetween(Double value1, Double value2) {
            addCriterion("MAPY between", value1, value2, "mapy");
            return (Criteria) this;
        }

        public Criteria andMapyNotBetween(Double value1, Double value2) {
            addCriterion("MAPY not between", value1, value2, "mapy");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("ADDRESS =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("ADDRESS <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("ADDRESS >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("ADDRESS <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("ADDRESS <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("ADDRESS like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("ADDRESS not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("ADDRESS in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("ADDRESS not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("ADDRESS between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("ADDRESS not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("IS_DEL is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("IS_DEL is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(String value) {
            addCriterion("IS_DEL =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(String value) {
            addCriterion("IS_DEL <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(String value) {
            addCriterion("IS_DEL >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(String value) {
            addCriterion("IS_DEL >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(String value) {
            addCriterion("IS_DEL <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(String value) {
            addCriterion("IS_DEL <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLike(String value) {
            addCriterion("IS_DEL like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotLike(String value) {
            addCriterion("IS_DEL not like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<String> values) {
            addCriterion("IS_DEL in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<String> values) {
            addCriterion("IS_DEL not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(String value1, String value2) {
            addCriterion("IS_DEL between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(String value1, String value2) {
            addCriterion("IS_DEL not between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDisIsNull() {
            addCriterion("IS_DIS is null");
            return (Criteria) this;
        }

        public Criteria andIsDisIsNotNull() {
            addCriterion("IS_DIS is not null");
            return (Criteria) this;
        }

        public Criteria andIsDisEqualTo(String value) {
            addCriterion("IS_DIS =", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisNotEqualTo(String value) {
            addCriterion("IS_DIS <>", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisGreaterThan(String value) {
            addCriterion("IS_DIS >", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisGreaterThanOrEqualTo(String value) {
            addCriterion("IS_DIS >=", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisLessThan(String value) {
            addCriterion("IS_DIS <", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisLessThanOrEqualTo(String value) {
            addCriterion("IS_DIS <=", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisLike(String value) {
            addCriterion("IS_DIS like", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisNotLike(String value) {
            addCriterion("IS_DIS not like", value, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisIn(List<String> values) {
            addCriterion("IS_DIS in", values, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisNotIn(List<String> values) {
            addCriterion("IS_DIS not in", values, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisBetween(String value1, String value2) {
            addCriterion("IS_DIS between", value1, value2, "isDis");
            return (Criteria) this;
        }

        public Criteria andIsDisNotBetween(String value1, String value2) {
            addCriterion("IS_DIS not between", value1, value2, "isDis");
            return (Criteria) this;
        }

        public Criteria andDelTimeIsNull() {
            addCriterion("DEL_TIME is null");
            return (Criteria) this;
        }

        public Criteria andDelTimeIsNotNull() {
            addCriterion("DEL_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andDelTimeEqualTo(String value) {
            addCriterion("DEL_TIME =", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeNotEqualTo(String value) {
            addCriterion("DEL_TIME <>", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeGreaterThan(String value) {
            addCriterion("DEL_TIME >", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeGreaterThanOrEqualTo(String value) {
            addCriterion("DEL_TIME >=", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeLessThan(String value) {
            addCriterion("DEL_TIME <", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeLessThanOrEqualTo(String value) {
            addCriterion("DEL_TIME <=", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeLike(String value) {
            addCriterion("DEL_TIME like", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeNotLike(String value) {
            addCriterion("DEL_TIME not like", value, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeIn(List<String> values) {
            addCriterion("DEL_TIME in", values, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeNotIn(List<String> values) {
            addCriterion("DEL_TIME not in", values, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeBetween(String value1, String value2) {
            addCriterion("DEL_TIME between", value1, value2, "delTime");
            return (Criteria) this;
        }

        public Criteria andDelTimeNotBetween(String value1, String value2) {
            addCriterion("DEL_TIME not between", value1, value2, "delTime");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("`STATE` is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("`STATE` is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("`STATE` =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("`STATE` <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("`STATE` >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("`STATE` >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("`STATE` <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("`STATE` <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("`STATE` like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("`STATE` not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("`STATE` in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("`STATE` not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("`STATE` between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("`STATE` not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("CREATETIME is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("CREATETIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(String value) {
            addCriterion("CREATETIME =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(String value) {
            addCriterion("CREATETIME <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(String value) {
            addCriterion("CREATETIME >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATETIME >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(String value) {
            addCriterion("CREATETIME <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(String value) {
            addCriterion("CREATETIME <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLike(String value) {
            addCriterion("CREATETIME like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotLike(String value) {
            addCriterion("CREATETIME not like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<String> values) {
            addCriterion("CREATETIME in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<String> values) {
            addCriterion("CREATETIME not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(String value1, String value2) {
            addCriterion("CREATETIME between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(String value1, String value2) {
            addCriterion("CREATETIME not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("USERNAME is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("USERNAME is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("USERNAME =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("USERNAME <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("USERNAME >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("USERNAME >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("USERNAME <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("USERNAME <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("USERNAME like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("USERNAME not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("USERNAME in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("USERNAME not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("USERNAME between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("USERNAME not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("`PASSWORD` is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("`PASSWORD` is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("`PASSWORD` =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("`PASSWORD` <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("`PASSWORD` >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("`PASSWORD` >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("`PASSWORD` <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("`PASSWORD` <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("`PASSWORD` like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("`PASSWORD` not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("`PASSWORD` in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("`PASSWORD` not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("`PASSWORD` between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("`PASSWORD` not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andImgIsNull() {
            addCriterion("IMG is null");
            return (Criteria) this;
        }

        public Criteria andImgIsNotNull() {
            addCriterion("IMG is not null");
            return (Criteria) this;
        }

        public Criteria andImgEqualTo(String value) {
            addCriterion("IMG =", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotEqualTo(String value) {
            addCriterion("IMG <>", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgGreaterThan(String value) {
            addCriterion("IMG >", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgGreaterThanOrEqualTo(String value) {
            addCriterion("IMG >=", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLessThan(String value) {
            addCriterion("IMG <", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLessThanOrEqualTo(String value) {
            addCriterion("IMG <=", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLike(String value) {
            addCriterion("IMG like", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotLike(String value) {
            addCriterion("IMG not like", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgIn(List<String> values) {
            addCriterion("IMG in", values, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotIn(List<String> values) {
            addCriterion("IMG not in", values, "img");
            return (Criteria) this;
        }

        public Criteria andImgBetween(String value1, String value2) {
            addCriterion("IMG between", value1, value2, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotBetween(String value1, String value2) {
            addCriterion("IMG not between", value1, value2, "img");
            return (Criteria) this;
        }

        public Criteria andCodeIsNull() {
            addCriterion("CODE is null");
            return (Criteria) this;
        }

        public Criteria andCodeIsNotNull() {
            addCriterion("CODE is not null");
            return (Criteria) this;
        }

        public Criteria andCodeEqualTo(String value) {
            addCriterion("CODE =", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("CODE <>", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThan(String value) {
            addCriterion("CODE >", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("CODE >=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThan(String value) {
            addCriterion("CODE <", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("CODE <=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLike(String value) {
            addCriterion("CODE like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotLike(String value) {
            addCriterion("CODE not like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeIn(List<String> values) {
            addCriterion("CODE in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("CODE not in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("CODE between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("CODE not between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andLinkmanIsNull() {
            addCriterion("LINKMAN is null");
            return (Criteria) this;
        }

        public Criteria andLinkmanIsNotNull() {
            addCriterion("LINKMAN is not null");
            return (Criteria) this;
        }

        public Criteria andLinkmanEqualTo(String value) {
            addCriterion("LINKMAN =", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotEqualTo(String value) {
            addCriterion("LINKMAN <>", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanGreaterThan(String value) {
            addCriterion("LINKMAN >", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanGreaterThanOrEqualTo(String value) {
            addCriterion("LINKMAN >=", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanLessThan(String value) {
            addCriterion("LINKMAN <", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanLessThanOrEqualTo(String value) {
            addCriterion("LINKMAN <=", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanLike(String value) {
            addCriterion("LINKMAN like", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotLike(String value) {
            addCriterion("LINKMAN not like", value, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanIn(List<String> values) {
            addCriterion("LINKMAN in", values, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotIn(List<String> values) {
            addCriterion("LINKMAN not in", values, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanBetween(String value1, String value2) {
            addCriterion("LINKMAN between", value1, value2, "linkman");
            return (Criteria) this;
        }

        public Criteria andLinkmanNotBetween(String value1, String value2) {
            addCriterion("LINKMAN not between", value1, value2, "linkman");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("MOBILE is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("MOBILE is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("MOBILE =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("MOBILE <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("MOBILE >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("MOBILE >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("MOBILE <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("MOBILE <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("MOBILE like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("MOBILE not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("MOBILE in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("MOBILE not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("MOBILE between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("MOBILE not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andServicesIsNull() {
            addCriterion("SERVICES is null");
            return (Criteria) this;
        }

        public Criteria andServicesIsNotNull() {
            addCriterion("SERVICES is not null");
            return (Criteria) this;
        }

        public Criteria andServicesEqualTo(String value) {
            addCriterion("SERVICES =", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesNotEqualTo(String value) {
            addCriterion("SERVICES <>", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesGreaterThan(String value) {
            addCriterion("SERVICES >", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesGreaterThanOrEqualTo(String value) {
            addCriterion("SERVICES >=", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesLessThan(String value) {
            addCriterion("SERVICES <", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesLessThanOrEqualTo(String value) {
            addCriterion("SERVICES <=", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesLike(String value) {
            addCriterion("SERVICES like", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesNotLike(String value) {
            addCriterion("SERVICES not like", value, "services");
            return (Criteria) this;
        }

        public Criteria andServicesIn(List<String> values) {
            addCriterion("SERVICES in", values, "services");
            return (Criteria) this;
        }

        public Criteria andServicesNotIn(List<String> values) {
            addCriterion("SERVICES not in", values, "services");
            return (Criteria) this;
        }

        public Criteria andServicesBetween(String value1, String value2) {
            addCriterion("SERVICES between", value1, value2, "services");
            return (Criteria) this;
        }

        public Criteria andServicesNotBetween(String value1, String value2) {
            addCriterion("SERVICES not between", value1, value2, "services");
            return (Criteria) this;
        }

        public Criteria andTimesIsNull() {
            addCriterion("TIMES is null");
            return (Criteria) this;
        }

        public Criteria andTimesIsNotNull() {
            addCriterion("TIMES is not null");
            return (Criteria) this;
        }

        public Criteria andTimesEqualTo(String value) {
            addCriterion("TIMES =", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesNotEqualTo(String value) {
            addCriterion("TIMES <>", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesGreaterThan(String value) {
            addCriterion("TIMES >", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesGreaterThanOrEqualTo(String value) {
            addCriterion("TIMES >=", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesLessThan(String value) {
            addCriterion("TIMES <", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesLessThanOrEqualTo(String value) {
            addCriterion("TIMES <=", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesLike(String value) {
            addCriterion("TIMES like", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesNotLike(String value) {
            addCriterion("TIMES not like", value, "times");
            return (Criteria) this;
        }

        public Criteria andTimesIn(List<String> values) {
            addCriterion("TIMES in", values, "times");
            return (Criteria) this;
        }

        public Criteria andTimesNotIn(List<String> values) {
            addCriterion("TIMES not in", values, "times");
            return (Criteria) this;
        }

        public Criteria andTimesBetween(String value1, String value2) {
            addCriterion("TIMES between", value1, value2, "times");
            return (Criteria) this;
        }

        public Criteria andTimesNotBetween(String value1, String value2) {
            addCriterion("TIMES not between", value1, value2, "times");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("EMAIL is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("EMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("EMAIL =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("EMAIL <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("EMAIL >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("EMAIL <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("EMAIL <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("EMAIL like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("EMAIL not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("EMAIL in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("EMAIL not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("EMAIL between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("EMAIL not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andTypesIsNull() {
            addCriterion("TYPES is null");
            return (Criteria) this;
        }

        public Criteria andTypesIsNotNull() {
            addCriterion("TYPES is not null");
            return (Criteria) this;
        }

        public Criteria andTypesEqualTo(String value) {
            addCriterion("TYPES =", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotEqualTo(String value) {
            addCriterion("TYPES <>", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesGreaterThan(String value) {
            addCriterion("TYPES >", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesGreaterThanOrEqualTo(String value) {
            addCriterion("TYPES >=", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesLessThan(String value) {
            addCriterion("TYPES <", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesLessThanOrEqualTo(String value) {
            addCriterion("TYPES <=", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesLike(String value) {
            addCriterion("TYPES like", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotLike(String value) {
            addCriterion("TYPES not like", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesIn(List<String> values) {
            addCriterion("TYPES in", values, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotIn(List<String> values) {
            addCriterion("TYPES not in", values, "types");
            return (Criteria) this;
        }

        public Criteria andTypesBetween(String value1, String value2) {
            addCriterion("TYPES between", value1, value2, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotBetween(String value1, String value2) {
            addCriterion("TYPES not between", value1, value2, "types");
            return (Criteria) this;
        }

        public Criteria andSysUserIdIsNull() {
            addCriterion("SYS_USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andSysUserIdIsNotNull() {
            addCriterion("SYS_USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSysUserIdEqualTo(String value) {
            addCriterion("SYS_USER_ID =", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdNotEqualTo(String value) {
            addCriterion("SYS_USER_ID <>", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdGreaterThan(String value) {
            addCriterion("SYS_USER_ID >", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("SYS_USER_ID >=", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdLessThan(String value) {
            addCriterion("SYS_USER_ID <", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdLessThanOrEqualTo(String value) {
            addCriterion("SYS_USER_ID <=", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdLike(String value) {
            addCriterion("SYS_USER_ID like", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdNotLike(String value) {
            addCriterion("SYS_USER_ID not like", value, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdIn(List<String> values) {
            addCriterion("SYS_USER_ID in", values, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdNotIn(List<String> values) {
            addCriterion("SYS_USER_ID not in", values, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdBetween(String value1, String value2) {
            addCriterion("SYS_USER_ID between", value1, value2, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andSysUserIdNotBetween(String value1, String value2) {
            addCriterion("SYS_USER_ID not between", value1, value2, "sysUserId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("AGENT_ID is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("AGENT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(String value) {
            addCriterion("AGENT_ID =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(String value) {
            addCriterion("AGENT_ID <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(String value) {
            addCriterion("AGENT_ID >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(String value) {
            addCriterion("AGENT_ID >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(String value) {
            addCriterion("AGENT_ID <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(String value) {
            addCriterion("AGENT_ID <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLike(String value) {
            addCriterion("AGENT_ID like", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotLike(String value) {
            addCriterion("AGENT_ID not like", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<String> values) {
            addCriterion("AGENT_ID in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<String> values) {
            addCriterion("AGENT_ID not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(String value1, String value2) {
            addCriterion("AGENT_ID between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(String value1, String value2) {
            addCriterion("AGENT_ID not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andQqIsNull() {
            addCriterion("QQ is null");
            return (Criteria) this;
        }

        public Criteria andQqIsNotNull() {
            addCriterion("QQ is not null");
            return (Criteria) this;
        }

        public Criteria andQqEqualTo(String value) {
            addCriterion("QQ =", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotEqualTo(String value) {
            addCriterion("QQ <>", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThan(String value) {
            addCriterion("QQ >", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThanOrEqualTo(String value) {
            addCriterion("QQ >=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThan(String value) {
            addCriterion("QQ <", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThanOrEqualTo(String value) {
            addCriterion("QQ <=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLike(String value) {
            addCriterion("QQ like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotLike(String value) {
            addCriterion("QQ not like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqIn(List<String> values) {
            addCriterion("QQ in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotIn(List<String> values) {
            addCriterion("QQ not in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqBetween(String value1, String value2) {
            addCriterion("QQ between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotBetween(String value1, String value2) {
            addCriterion("QQ not between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNull() {
            addCriterion("WEBSITE is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNotNull() {
            addCriterion("WEBSITE is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteEqualTo(String value) {
            addCriterion("WEBSITE =", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotEqualTo(String value) {
            addCriterion("WEBSITE <>", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThan(String value) {
            addCriterion("WEBSITE >", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThanOrEqualTo(String value) {
            addCriterion("WEBSITE >=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThan(String value) {
            addCriterion("WEBSITE <", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThanOrEqualTo(String value) {
            addCriterion("WEBSITE <=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLike(String value) {
            addCriterion("WEBSITE like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotLike(String value) {
            addCriterion("WEBSITE not like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteIn(List<String> values) {
            addCriterion("WEBSITE in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotIn(List<String> values) {
            addCriterion("WEBSITE not in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteBetween(String value1, String value2) {
            addCriterion("WEBSITE between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotBetween(String value1, String value2) {
            addCriterion("WEBSITE not between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andCatesIsNull() {
            addCriterion("CATES is null");
            return (Criteria) this;
        }

        public Criteria andCatesIsNotNull() {
            addCriterion("CATES is not null");
            return (Criteria) this;
        }

        public Criteria andCatesEqualTo(String value) {
            addCriterion("CATES =", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesNotEqualTo(String value) {
            addCriterion("CATES <>", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesGreaterThan(String value) {
            addCriterion("CATES >", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesGreaterThanOrEqualTo(String value) {
            addCriterion("CATES >=", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesLessThan(String value) {
            addCriterion("CATES <", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesLessThanOrEqualTo(String value) {
            addCriterion("CATES <=", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesLike(String value) {
            addCriterion("CATES like", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesNotLike(String value) {
            addCriterion("CATES not like", value, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesIn(List<String> values) {
            addCriterion("CATES in", values, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesNotIn(List<String> values) {
            addCriterion("CATES not in", values, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesBetween(String value1, String value2) {
            addCriterion("CATES between", value1, value2, "cates");
            return (Criteria) this;
        }

        public Criteria andCatesNotBetween(String value1, String value2) {
            addCriterion("CATES not between", value1, value2, "cates");
            return (Criteria) this;
        }

        public Criteria andIsTopIsNull() {
            addCriterion("IS_TOP is null");
            return (Criteria) this;
        }

        public Criteria andIsTopIsNotNull() {
            addCriterion("IS_TOP is not null");
            return (Criteria) this;
        }

        public Criteria andIsTopEqualTo(String value) {
            addCriterion("IS_TOP =", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotEqualTo(String value) {
            addCriterion("IS_TOP <>", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopGreaterThan(String value) {
            addCriterion("IS_TOP >", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopGreaterThanOrEqualTo(String value) {
            addCriterion("IS_TOP >=", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopLessThan(String value) {
            addCriterion("IS_TOP <", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopLessThanOrEqualTo(String value) {
            addCriterion("IS_TOP <=", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopLike(String value) {
            addCriterion("IS_TOP like", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotLike(String value) {
            addCriterion("IS_TOP not like", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopIn(List<String> values) {
            addCriterion("IS_TOP in", values, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotIn(List<String> values) {
            addCriterion("IS_TOP not in", values, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopBetween(String value1, String value2) {
            addCriterion("IS_TOP between", value1, value2, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotBetween(String value1, String value2) {
            addCriterion("IS_TOP not between", value1, value2, "isTop");
            return (Criteria) this;
        }

        public Criteria andCatesSecondIsNull() {
            addCriterion("CATES_SECOND is null");
            return (Criteria) this;
        }

        public Criteria andCatesSecondIsNotNull() {
            addCriterion("CATES_SECOND is not null");
            return (Criteria) this;
        }

        public Criteria andCatesSecondEqualTo(String value) {
            addCriterion("CATES_SECOND =", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondNotEqualTo(String value) {
            addCriterion("CATES_SECOND <>", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondGreaterThan(String value) {
            addCriterion("CATES_SECOND >", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondGreaterThanOrEqualTo(String value) {
            addCriterion("CATES_SECOND >=", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondLessThan(String value) {
            addCriterion("CATES_SECOND <", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondLessThanOrEqualTo(String value) {
            addCriterion("CATES_SECOND <=", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondLike(String value) {
            addCriterion("CATES_SECOND like", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondNotLike(String value) {
            addCriterion("CATES_SECOND not like", value, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondIn(List<String> values) {
            addCriterion("CATES_SECOND in", values, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondNotIn(List<String> values) {
            addCriterion("CATES_SECOND not in", values, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondBetween(String value1, String value2) {
            addCriterion("CATES_SECOND between", value1, value2, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andCatesSecondNotBetween(String value1, String value2) {
            addCriterion("CATES_SECOND not between", value1, value2, "catesSecond");
            return (Criteria) this;
        }

        public Criteria andTargetIdIsNull() {
            addCriterion("TARGET_ID is null");
            return (Criteria) this;
        }

        public Criteria andTargetIdIsNotNull() {
            addCriterion("TARGET_ID is not null");
            return (Criteria) this;
        }

        public Criteria andTargetIdEqualTo(String value) {
            addCriterion("TARGET_ID =", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdNotEqualTo(String value) {
            addCriterion("TARGET_ID <>", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdGreaterThan(String value) {
            addCriterion("TARGET_ID >", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdGreaterThanOrEqualTo(String value) {
            addCriterion("TARGET_ID >=", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdLessThan(String value) {
            addCriterion("TARGET_ID <", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdLessThanOrEqualTo(String value) {
            addCriterion("TARGET_ID <=", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdLike(String value) {
            addCriterion("TARGET_ID like", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdNotLike(String value) {
            addCriterion("TARGET_ID not like", value, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdIn(List<String> values) {
            addCriterion("TARGET_ID in", values, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdNotIn(List<String> values) {
            addCriterion("TARGET_ID not in", values, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdBetween(String value1, String value2) {
            addCriterion("TARGET_ID between", value1, value2, "targetId");
            return (Criteria) this;
        }

        public Criteria andTargetIdNotBetween(String value1, String value2) {
            addCriterion("TARGET_ID not between", value1, value2, "targetId");
            return (Criteria) this;
        }

        public Criteria andIsRightIsNull() {
            addCriterion("IS_RIGHT is null");
            return (Criteria) this;
        }

        public Criteria andIsRightIsNotNull() {
            addCriterion("IS_RIGHT is not null");
            return (Criteria) this;
        }

        public Criteria andIsRightEqualTo(String value) {
            addCriterion("IS_RIGHT =", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightNotEqualTo(String value) {
            addCriterion("IS_RIGHT <>", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightGreaterThan(String value) {
            addCriterion("IS_RIGHT >", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightGreaterThanOrEqualTo(String value) {
            addCriterion("IS_RIGHT >=", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightLessThan(String value) {
            addCriterion("IS_RIGHT <", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightLessThanOrEqualTo(String value) {
            addCriterion("IS_RIGHT <=", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightLike(String value) {
            addCriterion("IS_RIGHT like", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightNotLike(String value) {
            addCriterion("IS_RIGHT not like", value, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightIn(List<String> values) {
            addCriterion("IS_RIGHT in", values, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightNotIn(List<String> values) {
            addCriterion("IS_RIGHT not in", values, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightBetween(String value1, String value2) {
            addCriterion("IS_RIGHT between", value1, value2, "isRight");
            return (Criteria) this;
        }

        public Criteria andIsRightNotBetween(String value1, String value2) {
            addCriterion("IS_RIGHT not between", value1, value2, "isRight");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideIsNull() {
            addCriterion("WEBSITE_IS_HIDE is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideIsNotNull() {
            addCriterion("WEBSITE_IS_HIDE is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideEqualTo(String value) {
            addCriterion("WEBSITE_IS_HIDE =", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideNotEqualTo(String value) {
            addCriterion("WEBSITE_IS_HIDE <>", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideGreaterThan(String value) {
            addCriterion("WEBSITE_IS_HIDE >", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideGreaterThanOrEqualTo(String value) {
            addCriterion("WEBSITE_IS_HIDE >=", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideLessThan(String value) {
            addCriterion("WEBSITE_IS_HIDE <", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideLessThanOrEqualTo(String value) {
            addCriterion("WEBSITE_IS_HIDE <=", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideLike(String value) {
            addCriterion("WEBSITE_IS_HIDE like", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideNotLike(String value) {
            addCriterion("WEBSITE_IS_HIDE not like", value, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideIn(List<String> values) {
            addCriterion("WEBSITE_IS_HIDE in", values, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideNotIn(List<String> values) {
            addCriterion("WEBSITE_IS_HIDE not in", values, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideBetween(String value1, String value2) {
            addCriterion("WEBSITE_IS_HIDE between", value1, value2, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsHideNotBetween(String value1, String value2) {
            addCriterion("WEBSITE_IS_HIDE not between", value1, value2, "websiteIsHide");
            return (Criteria) this;
        }

        public Criteria andSwwgIsNull() {
            addCriterion("SWWG is null");
            return (Criteria) this;
        }

        public Criteria andSwwgIsNotNull() {
            addCriterion("SWWG is not null");
            return (Criteria) this;
        }

        public Criteria andSwwgEqualTo(Integer value) {
            addCriterion("SWWG =", value, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgNotEqualTo(Integer value) {
            addCriterion("SWWG <>", value, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgGreaterThan(Integer value) {
            addCriterion("SWWG >", value, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgGreaterThanOrEqualTo(Integer value) {
            addCriterion("SWWG >=", value, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgLessThan(Integer value) {
            addCriterion("SWWG <", value, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgLessThanOrEqualTo(Integer value) {
            addCriterion("SWWG <=", value, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgIn(List<Integer> values) {
            addCriterion("SWWG in", values, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgNotIn(List<Integer> values) {
            addCriterion("SWWG not in", values, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgBetween(Integer value1, Integer value2) {
            addCriterion("SWWG between", value1, value2, "swwg");
            return (Criteria) this;
        }

        public Criteria andSwwgNotBetween(Integer value1, Integer value2) {
            addCriterion("SWWG not between", value1, value2, "swwg");
            return (Criteria) this;
        }

        public Criteria andFwzlIsNull() {
            addCriterion("FWZL is null");
            return (Criteria) this;
        }

        public Criteria andFwzlIsNotNull() {
            addCriterion("FWZL is not null");
            return (Criteria) this;
        }

        public Criteria andFwzlEqualTo(Integer value) {
            addCriterion("FWZL =", value, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlNotEqualTo(Integer value) {
            addCriterion("FWZL <>", value, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlGreaterThan(Integer value) {
            addCriterion("FWZL >", value, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlGreaterThanOrEqualTo(Integer value) {
            addCriterion("FWZL >=", value, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlLessThan(Integer value) {
            addCriterion("FWZL <", value, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlLessThanOrEqualTo(Integer value) {
            addCriterion("FWZL <=", value, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlIn(List<Integer> values) {
            addCriterion("FWZL in", values, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlNotIn(List<Integer> values) {
            addCriterion("FWZL not in", values, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlBetween(Integer value1, Integer value2) {
            addCriterion("FWZL between", value1, value2, "fwzl");
            return (Criteria) this;
        }

        public Criteria andFwzlNotBetween(Integer value1, Integer value2) {
            addCriterion("FWZL not between", value1, value2, "fwzl");
            return (Criteria) this;
        }

        public Criteria andSpzlIsNull() {
            addCriterion("SPZL is null");
            return (Criteria) this;
        }

        public Criteria andSpzlIsNotNull() {
            addCriterion("SPZL is not null");
            return (Criteria) this;
        }

        public Criteria andSpzlEqualTo(Integer value) {
            addCriterion("SPZL =", value, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlNotEqualTo(Integer value) {
            addCriterion("SPZL <>", value, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlGreaterThan(Integer value) {
            addCriterion("SPZL >", value, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlGreaterThanOrEqualTo(Integer value) {
            addCriterion("SPZL >=", value, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlLessThan(Integer value) {
            addCriterion("SPZL <", value, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlLessThanOrEqualTo(Integer value) {
            addCriterion("SPZL <=", value, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlIn(List<Integer> values) {
            addCriterion("SPZL in", values, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlNotIn(List<Integer> values) {
            addCriterion("SPZL not in", values, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlBetween(Integer value1, Integer value2) {
            addCriterion("SPZL between", value1, value2, "spzl");
            return (Criteria) this;
        }

        public Criteria andSpzlNotBetween(Integer value1, Integer value2) {
            addCriterion("SPZL not between", value1, value2, "spzl");
            return (Criteria) this;
        }

        public Criteria andPsxlIsNull() {
            addCriterion("PSXL is null");
            return (Criteria) this;
        }

        public Criteria andPsxlIsNotNull() {
            addCriterion("PSXL is not null");
            return (Criteria) this;
        }

        public Criteria andPsxlEqualTo(Integer value) {
            addCriterion("PSXL =", value, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlNotEqualTo(Integer value) {
            addCriterion("PSXL <>", value, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlGreaterThan(Integer value) {
            addCriterion("PSXL >", value, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlGreaterThanOrEqualTo(Integer value) {
            addCriterion("PSXL >=", value, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlLessThan(Integer value) {
            addCriterion("PSXL <", value, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlLessThanOrEqualTo(Integer value) {
            addCriterion("PSXL <=", value, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlIn(List<Integer> values) {
            addCriterion("PSXL in", values, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlNotIn(List<Integer> values) {
            addCriterion("PSXL not in", values, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlBetween(Integer value1, Integer value2) {
            addCriterion("PSXL between", value1, value2, "psxl");
            return (Criteria) this;
        }

        public Criteria andPsxlNotBetween(Integer value1, Integer value2) {
            addCriterion("PSXL not between", value1, value2, "psxl");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("create_by is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("create_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(String value) {
            addCriterion("create_by =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(String value) {
            addCriterion("create_by <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(String value) {
            addCriterion("create_by >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(String value) {
            addCriterion("create_by >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(String value) {
            addCriterion("create_by <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(String value) {
            addCriterion("create_by <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLike(String value) {
            addCriterion("create_by like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotLike(String value) {
            addCriterion("create_by not like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<String> values) {
            addCriterion("create_by in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<String> values) {
            addCriterion("create_by not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(String value1, String value2) {
            addCriterion("create_by between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(String value1, String value2) {
            addCriterion("create_by not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andOrderSortIsNull() {
            addCriterion("order_sort is null");
            return (Criteria) this;
        }

        public Criteria andOrderSortIsNotNull() {
            addCriterion("order_sort is not null");
            return (Criteria) this;
        }

        public Criteria andOrderSortEqualTo(Integer value) {
            addCriterion("order_sort =", value, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortNotEqualTo(Integer value) {
            addCriterion("order_sort <>", value, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortGreaterThan(Integer value) {
            addCriterion("order_sort >", value, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_sort >=", value, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortLessThan(Integer value) {
            addCriterion("order_sort <", value, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortLessThanOrEqualTo(Integer value) {
            addCriterion("order_sort <=", value, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortIn(List<Integer> values) {
            addCriterion("order_sort in", values, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortNotIn(List<Integer> values) {
            addCriterion("order_sort not in", values, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortBetween(Integer value1, Integer value2) {
            addCriterion("order_sort between", value1, value2, "orderSort");
            return (Criteria) this;
        }

        public Criteria andOrderSortNotBetween(Integer value1, Integer value2) {
            addCriterion("order_sort not between", value1, value2, "orderSort");
            return (Criteria) this;
        }

        public Criteria andTelIsNull() {
            addCriterion("tel is null");
            return (Criteria) this;
        }

        public Criteria andTelIsNotNull() {
            addCriterion("tel is not null");
            return (Criteria) this;
        }

        public Criteria andTelEqualTo(String value) {
            addCriterion("tel =", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelNotEqualTo(String value) {
            addCriterion("tel <>", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelGreaterThan(String value) {
            addCriterion("tel >", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelGreaterThanOrEqualTo(String value) {
            addCriterion("tel >=", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelLessThan(String value) {
            addCriterion("tel <", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelLessThanOrEqualTo(String value) {
            addCriterion("tel <=", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelLike(String value) {
            addCriterion("tel like", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelNotLike(String value) {
            addCriterion("tel not like", value, "tel");
            return (Criteria) this;
        }

        public Criteria andTelIn(List<String> values) {
            addCriterion("tel in", values, "tel");
            return (Criteria) this;
        }

        public Criteria andTelNotIn(List<String> values) {
            addCriterion("tel not in", values, "tel");
            return (Criteria) this;
        }

        public Criteria andTelBetween(String value1, String value2) {
            addCriterion("tel between", value1, value2, "tel");
            return (Criteria) this;
        }

        public Criteria andTelNotBetween(String value1, String value2) {
            addCriterion("tel not between", value1, value2, "tel");
            return (Criteria) this;
        }

        public Criteria andPartnerIdIsNull() {
            addCriterion("PARTNER_ID is null");
            return (Criteria) this;
        }

        public Criteria andPartnerIdIsNotNull() {
            addCriterion("PARTNER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPartnerIdEqualTo(String value) {
            addCriterion("PARTNER_ID =", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdNotEqualTo(String value) {
            addCriterion("PARTNER_ID <>", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdGreaterThan(String value) {
            addCriterion("PARTNER_ID >", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdGreaterThanOrEqualTo(String value) {
            addCriterion("PARTNER_ID >=", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdLessThan(String value) {
            addCriterion("PARTNER_ID <", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdLessThanOrEqualTo(String value) {
            addCriterion("PARTNER_ID <=", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdLike(String value) {
            addCriterion("PARTNER_ID like", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdNotLike(String value) {
            addCriterion("PARTNER_ID not like", value, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdIn(List<String> values) {
            addCriterion("PARTNER_ID in", values, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdNotIn(List<String> values) {
            addCriterion("PARTNER_ID not in", values, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdBetween(String value1, String value2) {
            addCriterion("PARTNER_ID between", value1, value2, "partnerId");
            return (Criteria) this;
        }

        public Criteria andPartnerIdNotBetween(String value1, String value2) {
            addCriterion("PARTNER_ID not between", value1, value2, "partnerId");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("SORT is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("SORT is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("SORT =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("SORT <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("SORT >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("SORT >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("SORT <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("SORT <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("SORT in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("SORT not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("SORT between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("SORT not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andIsAdvIsNull() {
            addCriterion("is_adv is null");
            return (Criteria) this;
        }

        public Criteria andIsAdvIsNotNull() {
            addCriterion("is_adv is not null");
            return (Criteria) this;
        }

        public Criteria andIsAdvEqualTo(String value) {
            addCriterion("is_adv =", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvNotEqualTo(String value) {
            addCriterion("is_adv <>", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvGreaterThan(String value) {
            addCriterion("is_adv >", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvGreaterThanOrEqualTo(String value) {
            addCriterion("is_adv >=", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvLessThan(String value) {
            addCriterion("is_adv <", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvLessThanOrEqualTo(String value) {
            addCriterion("is_adv <=", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvLike(String value) {
            addCriterion("is_adv like", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvNotLike(String value) {
            addCriterion("is_adv not like", value, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvIn(List<String> values) {
            addCriterion("is_adv in", values, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvNotIn(List<String> values) {
            addCriterion("is_adv not in", values, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvBetween(String value1, String value2) {
            addCriterion("is_adv between", value1, value2, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAdvNotBetween(String value1, String value2) {
            addCriterion("is_adv not between", value1, value2, "isAdv");
            return (Criteria) this;
        }

        public Criteria andIsAppoIsNull() {
            addCriterion("is_appo is null");
            return (Criteria) this;
        }

        public Criteria andIsAppoIsNotNull() {
            addCriterion("is_appo is not null");
            return (Criteria) this;
        }

        public Criteria andIsAppoEqualTo(String value) {
            addCriterion("is_appo =", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoNotEqualTo(String value) {
            addCriterion("is_appo <>", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoGreaterThan(String value) {
            addCriterion("is_appo >", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoGreaterThanOrEqualTo(String value) {
            addCriterion("is_appo >=", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoLessThan(String value) {
            addCriterion("is_appo <", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoLessThanOrEqualTo(String value) {
            addCriterion("is_appo <=", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoLike(String value) {
            addCriterion("is_appo like", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoNotLike(String value) {
            addCriterion("is_appo not like", value, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoIn(List<String> values) {
            addCriterion("is_appo in", values, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoNotIn(List<String> values) {
            addCriterion("is_appo not in", values, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoBetween(String value1, String value2) {
            addCriterion("is_appo between", value1, value2, "isAppo");
            return (Criteria) this;
        }

        public Criteria andIsAppoNotBetween(String value1, String value2) {
            addCriterion("is_appo not between", value1, value2, "isAppo");
            return (Criteria) this;
        }

        public Criteria andSmsMobileIsNull() {
            addCriterion("sms_mobile is null");
            return (Criteria) this;
        }

        public Criteria andSmsMobileIsNotNull() {
            addCriterion("sms_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andSmsMobileEqualTo(String value) {
            addCriterion("sms_mobile =", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileNotEqualTo(String value) {
            addCriterion("sms_mobile <>", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileGreaterThan(String value) {
            addCriterion("sms_mobile >", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileGreaterThanOrEqualTo(String value) {
            addCriterion("sms_mobile >=", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileLessThan(String value) {
            addCriterion("sms_mobile <", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileLessThanOrEqualTo(String value) {
            addCriterion("sms_mobile <=", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileLike(String value) {
            addCriterion("sms_mobile like", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileNotLike(String value) {
            addCriterion("sms_mobile not like", value, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileIn(List<String> values) {
            addCriterion("sms_mobile in", values, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileNotIn(List<String> values) {
            addCriterion("sms_mobile not in", values, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileBetween(String value1, String value2) {
            addCriterion("sms_mobile between", value1, value2, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andSmsMobileNotBetween(String value1, String value2) {
            addCriterion("sms_mobile not between", value1, value2, "smsMobile");
            return (Criteria) this;
        }

        public Criteria andFenleiIdIsNull() {
            addCriterion("fenlei_id is null");
            return (Criteria) this;
        }

        public Criteria andFenleiIdIsNotNull() {
            addCriterion("fenlei_id is not null");
            return (Criteria) this;
        }

        public Criteria andFenleiIdEqualTo(String value) {
            addCriterion("fenlei_id =", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdNotEqualTo(String value) {
            addCriterion("fenlei_id <>", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdGreaterThan(String value) {
            addCriterion("fenlei_id >", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdGreaterThanOrEqualTo(String value) {
            addCriterion("fenlei_id >=", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdLessThan(String value) {
            addCriterion("fenlei_id <", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdLessThanOrEqualTo(String value) {
            addCriterion("fenlei_id <=", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdLike(String value) {
            addCriterion("fenlei_id like", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdNotLike(String value) {
            addCriterion("fenlei_id not like", value, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdIn(List<String> values) {
            addCriterion("fenlei_id in", values, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdNotIn(List<String> values) {
            addCriterion("fenlei_id not in", values, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdBetween(String value1, String value2) {
            addCriterion("fenlei_id between", value1, value2, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andFenleiIdNotBetween(String value1, String value2) {
            addCriterion("fenlei_id not between", value1, value2, "fenleiId");
            return (Criteria) this;
        }

        public Criteria andIsAdminIsNull() {
            addCriterion("is_admin is null");
            return (Criteria) this;
        }

        public Criteria andIsAdminIsNotNull() {
            addCriterion("is_admin is not null");
            return (Criteria) this;
        }

        public Criteria andIsAdminEqualTo(String value) {
            addCriterion("is_admin =", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminNotEqualTo(String value) {
            addCriterion("is_admin <>", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminGreaterThan(String value) {
            addCriterion("is_admin >", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminGreaterThanOrEqualTo(String value) {
            addCriterion("is_admin >=", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminLessThan(String value) {
            addCriterion("is_admin <", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminLessThanOrEqualTo(String value) {
            addCriterion("is_admin <=", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminLike(String value) {
            addCriterion("is_admin like", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminNotLike(String value) {
            addCriterion("is_admin not like", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminIn(List<String> values) {
            addCriterion("is_admin in", values, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminNotIn(List<String> values) {
            addCriterion("is_admin not in", values, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminBetween(String value1, String value2) {
            addCriterion("is_admin between", value1, value2, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminNotBetween(String value1, String value2) {
            addCriterion("is_admin not between", value1, value2, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andFollowNumIsNull() {
            addCriterion("follow_num is null");
            return (Criteria) this;
        }

        public Criteria andFollowNumIsNotNull() {
            addCriterion("follow_num is not null");
            return (Criteria) this;
        }

        public Criteria andFollowNumEqualTo(Integer value) {
            addCriterion("follow_num =", value, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumNotEqualTo(Integer value) {
            addCriterion("follow_num <>", value, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumGreaterThan(Integer value) {
            addCriterion("follow_num >", value, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("follow_num >=", value, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumLessThan(Integer value) {
            addCriterion("follow_num <", value, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumLessThanOrEqualTo(Integer value) {
            addCriterion("follow_num <=", value, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumIn(List<Integer> values) {
            addCriterion("follow_num in", values, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumNotIn(List<Integer> values) {
            addCriterion("follow_num not in", values, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumBetween(Integer value1, Integer value2) {
            addCriterion("follow_num between", value1, value2, "followNum");
            return (Criteria) this;
        }

        public Criteria andFollowNumNotBetween(Integer value1, Integer value2) {
            addCriterion("follow_num not between", value1, value2, "followNum");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNull() {
            addCriterion("source_id is null");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNotNull() {
            addCriterion("source_id is not null");
            return (Criteria) this;
        }

        public Criteria andSourceIdEqualTo(String value) {
            addCriterion("source_id =", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotEqualTo(String value) {
            addCriterion("source_id <>", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThan(String value) {
            addCriterion("source_id >", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThanOrEqualTo(String value) {
            addCriterion("source_id >=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThan(String value) {
            addCriterion("source_id <", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThanOrEqualTo(String value) {
            addCriterion("source_id <=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLike(String value) {
            addCriterion("source_id like", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotLike(String value) {
            addCriterion("source_id not like", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdIn(List<String> values) {
            addCriterion("source_id in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotIn(List<String> values) {
            addCriterion("source_id not in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdBetween(String value1, String value2) {
            addCriterion("source_id between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotBetween(String value1, String value2) {
            addCriterion("source_id not between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdIsNull() {
            addCriterion("dada_shop_id is null");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdIsNotNull() {
            addCriterion("dada_shop_id is not null");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdEqualTo(String value) {
            addCriterion("dada_shop_id =", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdNotEqualTo(String value) {
            addCriterion("dada_shop_id <>", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdGreaterThan(String value) {
            addCriterion("dada_shop_id >", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdGreaterThanOrEqualTo(String value) {
            addCriterion("dada_shop_id >=", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdLessThan(String value) {
            addCriterion("dada_shop_id <", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdLessThanOrEqualTo(String value) {
            addCriterion("dada_shop_id <=", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdLike(String value) {
            addCriterion("dada_shop_id like", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdNotLike(String value) {
            addCriterion("dada_shop_id not like", value, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdIn(List<String> values) {
            addCriterion("dada_shop_id in", values, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdNotIn(List<String> values) {
            addCriterion("dada_shop_id not in", values, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdBetween(String value1, String value2) {
            addCriterion("dada_shop_id between", value1, value2, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andDadaShopIdNotBetween(String value1, String value2) {
            addCriterion("dada_shop_id not between", value1, value2, "dadaShopId");
            return (Criteria) this;
        }

        public Criteria andIsSynchroIsNull() {
            addCriterion("is_synchro is null");
            return (Criteria) this;
        }

        public Criteria andIsSynchroIsNotNull() {
            addCriterion("is_synchro is not null");
            return (Criteria) this;
        }

        public Criteria andIsSynchroEqualTo(String value) {
            addCriterion("is_synchro =", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroNotEqualTo(String value) {
            addCriterion("is_synchro <>", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroGreaterThan(String value) {
            addCriterion("is_synchro >", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroGreaterThanOrEqualTo(String value) {
            addCriterion("is_synchro >=", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroLessThan(String value) {
            addCriterion("is_synchro <", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroLessThanOrEqualTo(String value) {
            addCriterion("is_synchro <=", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroLike(String value) {
            addCriterion("is_synchro like", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroNotLike(String value) {
            addCriterion("is_synchro not like", value, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroIn(List<String> values) {
            addCriterion("is_synchro in", values, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroNotIn(List<String> values) {
            addCriterion("is_synchro not in", values, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroBetween(String value1, String value2) {
            addCriterion("is_synchro between", value1, value2, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsSynchroNotBetween(String value1, String value2) {
            addCriterion("is_synchro not between", value1, value2, "isSynchro");
            return (Criteria) this;
        }

        public Criteria andIsDadaIsNull() {
            addCriterion("is_dada is null");
            return (Criteria) this;
        }

        public Criteria andIsDadaIsNotNull() {
            addCriterion("is_dada is not null");
            return (Criteria) this;
        }

        public Criteria andIsDadaEqualTo(String value) {
            addCriterion("is_dada =", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaNotEqualTo(String value) {
            addCriterion("is_dada <>", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaGreaterThan(String value) {
            addCriterion("is_dada >", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaGreaterThanOrEqualTo(String value) {
            addCriterion("is_dada >=", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaLessThan(String value) {
            addCriterion("is_dada <", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaLessThanOrEqualTo(String value) {
            addCriterion("is_dada <=", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaLike(String value) {
            addCriterion("is_dada like", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaNotLike(String value) {
            addCriterion("is_dada not like", value, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaIn(List<String> values) {
            addCriterion("is_dada in", values, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaNotIn(List<String> values) {
            addCriterion("is_dada not in", values, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaBetween(String value1, String value2) {
            addCriterion("is_dada between", value1, value2, "isDada");
            return (Criteria) this;
        }

        public Criteria andIsDadaNotBetween(String value1, String value2) {
            addCriterion("is_dada not between", value1, value2, "isDada");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameIsNull() {
            addCriterion("dada_city_name is null");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameIsNotNull() {
            addCriterion("dada_city_name is not null");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameEqualTo(String value) {
            addCriterion("dada_city_name =", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameNotEqualTo(String value) {
            addCriterion("dada_city_name <>", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameGreaterThan(String value) {
            addCriterion("dada_city_name >", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("dada_city_name >=", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameLessThan(String value) {
            addCriterion("dada_city_name <", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameLessThanOrEqualTo(String value) {
            addCriterion("dada_city_name <=", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameLike(String value) {
            addCriterion("dada_city_name like", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameNotLike(String value) {
            addCriterion("dada_city_name not like", value, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameIn(List<String> values) {
            addCriterion("dada_city_name in", values, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameNotIn(List<String> values) {
            addCriterion("dada_city_name not in", values, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameBetween(String value1, String value2) {
            addCriterion("dada_city_name between", value1, value2, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaCityNameNotBetween(String value1, String value2) {
            addCriterion("dada_city_name not between", value1, value2, "dadaCityName");
            return (Criteria) this;
        }

        public Criteria andDadaPwdIsNull() {
            addCriterion("dada_pwd is null");
            return (Criteria) this;
        }

        public Criteria andDadaPwdIsNotNull() {
            addCriterion("dada_pwd is not null");
            return (Criteria) this;
        }

        public Criteria andDadaPwdEqualTo(String value) {
            addCriterion("dada_pwd =", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdNotEqualTo(String value) {
            addCriterion("dada_pwd <>", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdGreaterThan(String value) {
            addCriterion("dada_pwd >", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdGreaterThanOrEqualTo(String value) {
            addCriterion("dada_pwd >=", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdLessThan(String value) {
            addCriterion("dada_pwd <", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdLessThanOrEqualTo(String value) {
            addCriterion("dada_pwd <=", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdLike(String value) {
            addCriterion("dada_pwd like", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdNotLike(String value) {
            addCriterion("dada_pwd not like", value, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdIn(List<String> values) {
            addCriterion("dada_pwd in", values, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdNotIn(List<String> values) {
            addCriterion("dada_pwd not in", values, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdBetween(String value1, String value2) {
            addCriterion("dada_pwd between", value1, value2, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andDadaPwdNotBetween(String value1, String value2) {
            addCriterion("dada_pwd not between", value1, value2, "dadaPwd");
            return (Criteria) this;
        }

        public Criteria andIsYingyeIsNull() {
            addCriterion("is_yingye is null");
            return (Criteria) this;
        }

        public Criteria andIsYingyeIsNotNull() {
            addCriterion("is_yingye is not null");
            return (Criteria) this;
        }

        public Criteria andIsYingyeEqualTo(String value) {
            addCriterion("is_yingye =", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeNotEqualTo(String value) {
            addCriterion("is_yingye <>", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeGreaterThan(String value) {
            addCriterion("is_yingye >", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeGreaterThanOrEqualTo(String value) {
            addCriterion("is_yingye >=", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeLessThan(String value) {
            addCriterion("is_yingye <", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeLessThanOrEqualTo(String value) {
            addCriterion("is_yingye <=", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeLike(String value) {
            addCriterion("is_yingye like", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeNotLike(String value) {
            addCriterion("is_yingye not like", value, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeIn(List<String> values) {
            addCriterion("is_yingye in", values, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeNotIn(List<String> values) {
            addCriterion("is_yingye not in", values, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeBetween(String value1, String value2) {
            addCriterion("is_yingye between", value1, value2, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsYingyeNotBetween(String value1, String value2) {
            addCriterion("is_yingye not between", value1, value2, "isYingye");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopIsNull() {
            addCriterion("is_takeout_top is null");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopIsNotNull() {
            addCriterion("is_takeout_top is not null");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopEqualTo(String value) {
            addCriterion("is_takeout_top =", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopNotEqualTo(String value) {
            addCriterion("is_takeout_top <>", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopGreaterThan(String value) {
            addCriterion("is_takeout_top >", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopGreaterThanOrEqualTo(String value) {
            addCriterion("is_takeout_top >=", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopLessThan(String value) {
            addCriterion("is_takeout_top <", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopLessThanOrEqualTo(String value) {
            addCriterion("is_takeout_top <=", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopLike(String value) {
            addCriterion("is_takeout_top like", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopNotLike(String value) {
            addCriterion("is_takeout_top not like", value, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopIn(List<String> values) {
            addCriterion("is_takeout_top in", values, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopNotIn(List<String> values) {
            addCriterion("is_takeout_top not in", values, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopBetween(String value1, String value2) {
            addCriterion("is_takeout_top between", value1, value2, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsTakeoutTopNotBetween(String value1, String value2) {
            addCriterion("is_takeout_top not between", value1, value2, "isTakeoutTop");
            return (Criteria) this;
        }

        public Criteria andIsEfpIsNull() {
            addCriterion("is_efp is null");
            return (Criteria) this;
        }

        public Criteria andIsEfpIsNotNull() {
            addCriterion("is_efp is not null");
            return (Criteria) this;
        }

        public Criteria andIsEfpEqualTo(String value) {
            addCriterion("is_efp =", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpNotEqualTo(String value) {
            addCriterion("is_efp <>", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpGreaterThan(String value) {
            addCriterion("is_efp >", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpGreaterThanOrEqualTo(String value) {
            addCriterion("is_efp >=", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpLessThan(String value) {
            addCriterion("is_efp <", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpLessThanOrEqualTo(String value) {
            addCriterion("is_efp <=", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpLike(String value) {
            addCriterion("is_efp like", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpNotLike(String value) {
            addCriterion("is_efp not like", value, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpIn(List<String> values) {
            addCriterion("is_efp in", values, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpNotIn(List<String> values) {
            addCriterion("is_efp not in", values, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpBetween(String value1, String value2) {
            addCriterion("is_efp between", value1, value2, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsEfpNotBetween(String value1, String value2) {
            addCriterion("is_efp not between", value1, value2, "isEfp");
            return (Criteria) this;
        }

        public Criteria andIsPcTopIsNull() {
            addCriterion("is_pc_top is null");
            return (Criteria) this;
        }

        public Criteria andIsPcTopIsNotNull() {
            addCriterion("is_pc_top is not null");
            return (Criteria) this;
        }

        public Criteria andIsPcTopEqualTo(String value) {
            addCriterion("is_pc_top =", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopNotEqualTo(String value) {
            addCriterion("is_pc_top <>", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopGreaterThan(String value) {
            addCriterion("is_pc_top >", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopGreaterThanOrEqualTo(String value) {
            addCriterion("is_pc_top >=", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopLessThan(String value) {
            addCriterion("is_pc_top <", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopLessThanOrEqualTo(String value) {
            addCriterion("is_pc_top <=", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopLike(String value) {
            addCriterion("is_pc_top like", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopNotLike(String value) {
            addCriterion("is_pc_top not like", value, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopIn(List<String> values) {
            addCriterion("is_pc_top in", values, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopNotIn(List<String> values) {
            addCriterion("is_pc_top not in", values, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopBetween(String value1, String value2) {
            addCriterion("is_pc_top between", value1, value2, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsPcTopNotBetween(String value1, String value2) {
            addCriterion("is_pc_top not between", value1, value2, "isPcTop");
            return (Criteria) this;
        }

        public Criteria andIsBannerIsNull() {
            addCriterion("is_banner is null");
            return (Criteria) this;
        }

        public Criteria andIsBannerIsNotNull() {
            addCriterion("is_banner is not null");
            return (Criteria) this;
        }

        public Criteria andIsBannerEqualTo(String value) {
            addCriterion("is_banner =", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerNotEqualTo(String value) {
            addCriterion("is_banner <>", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerGreaterThan(String value) {
            addCriterion("is_banner >", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerGreaterThanOrEqualTo(String value) {
            addCriterion("is_banner >=", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerLessThan(String value) {
            addCriterion("is_banner <", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerLessThanOrEqualTo(String value) {
            addCriterion("is_banner <=", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerLike(String value) {
            addCriterion("is_banner like", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerNotLike(String value) {
            addCriterion("is_banner not like", value, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerIn(List<String> values) {
            addCriterion("is_banner in", values, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerNotIn(List<String> values) {
            addCriterion("is_banner not in", values, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerBetween(String value1, String value2) {
            addCriterion("is_banner between", value1, value2, "isBanner");
            return (Criteria) this;
        }

        public Criteria andIsBannerNotBetween(String value1, String value2) {
            addCriterion("is_banner not between", value1, value2, "isBanner");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andRefundAddressIsNull() {
            addCriterion("refund_address is null");
            return (Criteria) this;
        }

        public Criteria andRefundAddressIsNotNull() {
            addCriterion("refund_address is not null");
            return (Criteria) this;
        }

        public Criteria andRefundAddressEqualTo(String value) {
            addCriterion("refund_address =", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotEqualTo(String value) {
            addCriterion("refund_address <>", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressGreaterThan(String value) {
            addCriterion("refund_address >", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressGreaterThanOrEqualTo(String value) {
            addCriterion("refund_address >=", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressLessThan(String value) {
            addCriterion("refund_address <", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressLessThanOrEqualTo(String value) {
            addCriterion("refund_address <=", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressLike(String value) {
            addCriterion("refund_address like", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotLike(String value) {
            addCriterion("refund_address not like", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressIn(List<String> values) {
            addCriterion("refund_address in", values, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotIn(List<String> values) {
            addCriterion("refund_address not in", values, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressBetween(String value1, String value2) {
            addCriterion("refund_address between", value1, value2, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotBetween(String value1, String value2) {
            addCriterion("refund_address not between", value1, value2, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanIsNull() {
            addCriterion("refund_linkman is null");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanIsNotNull() {
            addCriterion("refund_linkman is not null");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanEqualTo(String value) {
            addCriterion("refund_linkman =", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanNotEqualTo(String value) {
            addCriterion("refund_linkman <>", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanGreaterThan(String value) {
            addCriterion("refund_linkman >", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanGreaterThanOrEqualTo(String value) {
            addCriterion("refund_linkman >=", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanLessThan(String value) {
            addCriterion("refund_linkman <", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanLessThanOrEqualTo(String value) {
            addCriterion("refund_linkman <=", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanLike(String value) {
            addCriterion("refund_linkman like", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanNotLike(String value) {
            addCriterion("refund_linkman not like", value, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanIn(List<String> values) {
            addCriterion("refund_linkman in", values, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanNotIn(List<String> values) {
            addCriterion("refund_linkman not in", values, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanBetween(String value1, String value2) {
            addCriterion("refund_linkman between", value1, value2, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundLinkmanNotBetween(String value1, String value2) {
            addCriterion("refund_linkman not between", value1, value2, "refundLinkman");
            return (Criteria) this;
        }

        public Criteria andRefundMobileIsNull() {
            addCriterion("refund_mobile is null");
            return (Criteria) this;
        }

        public Criteria andRefundMobileIsNotNull() {
            addCriterion("refund_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andRefundMobileEqualTo(String value) {
            addCriterion("refund_mobile =", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileNotEqualTo(String value) {
            addCriterion("refund_mobile <>", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileGreaterThan(String value) {
            addCriterion("refund_mobile >", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileGreaterThanOrEqualTo(String value) {
            addCriterion("refund_mobile >=", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileLessThan(String value) {
            addCriterion("refund_mobile <", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileLessThanOrEqualTo(String value) {
            addCriterion("refund_mobile <=", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileLike(String value) {
            addCriterion("refund_mobile like", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileNotLike(String value) {
            addCriterion("refund_mobile not like", value, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileIn(List<String> values) {
            addCriterion("refund_mobile in", values, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileNotIn(List<String> values) {
            addCriterion("refund_mobile not in", values, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileBetween(String value1, String value2) {
            addCriterion("refund_mobile between", value1, value2, "refundMobile");
            return (Criteria) this;
        }

        public Criteria andRefundMobileNotBetween(String value1, String value2) {
            addCriterion("refund_mobile not between", value1, value2, "refundMobile");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
