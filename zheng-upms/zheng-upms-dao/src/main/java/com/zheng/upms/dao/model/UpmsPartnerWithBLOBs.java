package com.zheng.upms.dao.model;

import java.io.Serializable;

/**
 * upms_partner
 * @author
 */
public class UpmsPartnerWithBLOBs extends UpmsPartner implements Serializable {
    /**
     * 简介
     */
    private String shl;

    /**
     * 详情
     */
    private String detail;

    /**
     * 服务项目
     */
    private String serPro;

    private String shlId;

    /**
     * 分类id
     */
    private String typeId;

    private static final long serialVersionUID = 1L;

    public String getShl() {
        return shl;
    }

    public void setShl(String shl) {
        this.shl = shl;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getSerPro() {
        return serPro;
    }

    public void setSerPro(String serPro) {
        this.serPro = serPro;
    }

    public String getShlId() {
        return shlId;
    }

    public void setShlId(String shlId) {
        this.shlId = shlId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UpmsPartnerWithBLOBs other = (UpmsPartnerWithBLOBs) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getProvinceId() == null ? other.getProvinceId() == null : this.getProvinceId().equals(other.getProvinceId()))
            && (this.getCityId() == null ? other.getCityId() == null : this.getCityId().equals(other.getCityId()))
            && (this.getCountyId() == null ? other.getCountyId() == null : this.getCountyId().equals(other.getCountyId()))
            && (this.getProvinceName() == null ? other.getProvinceName() == null : this.getProvinceName().equals(other.getProvinceName()))
            && (this.getCityName() == null ? other.getCityName() == null : this.getCityName().equals(other.getCityName()))
            && (this.getCountyName() == null ? other.getCountyName() == null : this.getCountyName().equals(other.getCountyName()))
            && (this.getMapx() == null ? other.getMapx() == null : this.getMapx().equals(other.getMapx()))
            && (this.getMapy() == null ? other.getMapy() == null : this.getMapy().equals(other.getMapy()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getIsDel() == null ? other.getIsDel() == null : this.getIsDel().equals(other.getIsDel()))
            && (this.getIsDis() == null ? other.getIsDis() == null : this.getIsDis().equals(other.getIsDis()))
            && (this.getDelTime() == null ? other.getDelTime() == null : this.getDelTime().equals(other.getDelTime()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getImg() == null ? other.getImg() == null : this.getImg().equals(other.getImg()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getLinkman() == null ? other.getLinkman() == null : this.getLinkman().equals(other.getLinkman()))
            && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()))
            && (this.getServices() == null ? other.getServices() == null : this.getServices().equals(other.getServices()))
            && (this.getTimes() == null ? other.getTimes() == null : this.getTimes().equals(other.getTimes()))
            && (this.getEmail() == null ? other.getEmail() == null : this.getEmail().equals(other.getEmail()))
            && (this.getTypes() == null ? other.getTypes() == null : this.getTypes().equals(other.getTypes()))
            && (this.getSysUserId() == null ? other.getSysUserId() == null : this.getSysUserId().equals(other.getSysUserId()))
            && (this.getAgentId() == null ? other.getAgentId() == null : this.getAgentId().equals(other.getAgentId()))
            && (this.getQq() == null ? other.getQq() == null : this.getQq().equals(other.getQq()))
            && (this.getWebsite() == null ? other.getWebsite() == null : this.getWebsite().equals(other.getWebsite()))
            && (this.getCates() == null ? other.getCates() == null : this.getCates().equals(other.getCates()))
            && (this.getIsTop() == null ? other.getIsTop() == null : this.getIsTop().equals(other.getIsTop()))
            && (this.getCatesSecond() == null ? other.getCatesSecond() == null : this.getCatesSecond().equals(other.getCatesSecond()))
            && (this.getTargetId() == null ? other.getTargetId() == null : this.getTargetId().equals(other.getTargetId()))
            && (this.getIsRight() == null ? other.getIsRight() == null : this.getIsRight().equals(other.getIsRight()))
            && (this.getWebsiteIsHide() == null ? other.getWebsiteIsHide() == null : this.getWebsiteIsHide().equals(other.getWebsiteIsHide()))
            && (this.getSwwg() == null ? other.getSwwg() == null : this.getSwwg().equals(other.getSwwg()))
            && (this.getFwzl() == null ? other.getFwzl() == null : this.getFwzl().equals(other.getFwzl()))
            && (this.getSpzl() == null ? other.getSpzl() == null : this.getSpzl().equals(other.getSpzl()))
            && (this.getPsxl() == null ? other.getPsxl() == null : this.getPsxl().equals(other.getPsxl()))
            && (this.getCreateDate() == null ? other.getCreateDate() == null : this.getCreateDate().equals(other.getCreateDate()))
            && (this.getCreateBy() == null ? other.getCreateBy() == null : this.getCreateBy().equals(other.getCreateBy()))
            && (this.getOrderSort() == null ? other.getOrderSort() == null : this.getOrderSort().equals(other.getOrderSort()))
            && (this.getTel() == null ? other.getTel() == null : this.getTel().equals(other.getTel()))
            && (this.getPartnerId() == null ? other.getPartnerId() == null : this.getPartnerId().equals(other.getPartnerId()))
            && (this.getSort() == null ? other.getSort() == null : this.getSort().equals(other.getSort()))
            && (this.getIsAdv() == null ? other.getIsAdv() == null : this.getIsAdv().equals(other.getIsAdv()))
            && (this.getIsAppo() == null ? other.getIsAppo() == null : this.getIsAppo().equals(other.getIsAppo()))
            && (this.getSmsMobile() == null ? other.getSmsMobile() == null : this.getSmsMobile().equals(other.getSmsMobile()))
            && (this.getFenleiId() == null ? other.getFenleiId() == null : this.getFenleiId().equals(other.getFenleiId()))
            && (this.getIsAdmin() == null ? other.getIsAdmin() == null : this.getIsAdmin().equals(other.getIsAdmin()))
            && (this.getFollowNum() == null ? other.getFollowNum() == null : this.getFollowNum().equals(other.getFollowNum()))
            && (this.getSourceId() == null ? other.getSourceId() == null : this.getSourceId().equals(other.getSourceId()))
            && (this.getDadaShopId() == null ? other.getDadaShopId() == null : this.getDadaShopId().equals(other.getDadaShopId()))
            && (this.getIsSynchro() == null ? other.getIsSynchro() == null : this.getIsSynchro().equals(other.getIsSynchro()))
            && (this.getIsDada() == null ? other.getIsDada() == null : this.getIsDada().equals(other.getIsDada()))
            && (this.getDadaCityName() == null ? other.getDadaCityName() == null : this.getDadaCityName().equals(other.getDadaCityName()))
            && (this.getDadaPwd() == null ? other.getDadaPwd() == null : this.getDadaPwd().equals(other.getDadaPwd()))
            && (this.getIsYingye() == null ? other.getIsYingye() == null : this.getIsYingye().equals(other.getIsYingye()))
            && (this.getIsTakeoutTop() == null ? other.getIsTakeoutTop() == null : this.getIsTakeoutTop().equals(other.getIsTakeoutTop()))
            && (this.getIsEfp() == null ? other.getIsEfp() == null : this.getIsEfp().equals(other.getIsEfp()))
            && (this.getIsPcTop() == null ? other.getIsPcTop() == null : this.getIsPcTop().equals(other.getIsPcTop()))
            && (this.getIsBanner() == null ? other.getIsBanner() == null : this.getIsBanner().equals(other.getIsBanner()))
            && (this.getUpdateDate() == null ? other.getUpdateDate() == null : this.getUpdateDate().equals(other.getUpdateDate()))
            && (this.getRefundAddress() == null ? other.getRefundAddress() == null : this.getRefundAddress().equals(other.getRefundAddress()))
            && (this.getRefundLinkman() == null ? other.getRefundLinkman() == null : this.getRefundLinkman().equals(other.getRefundLinkman()))
            && (this.getRefundMobile() == null ? other.getRefundMobile() == null : this.getRefundMobile().equals(other.getRefundMobile()))
            && (this.getShl() == null ? other.getShl() == null : this.getShl().equals(other.getShl()))
            && (this.getDetail() == null ? other.getDetail() == null : this.getDetail().equals(other.getDetail()))
            && (this.getSerPro() == null ? other.getSerPro() == null : this.getSerPro().equals(other.getSerPro()))
            && (this.getShlId() == null ? other.getShlId() == null : this.getShlId().equals(other.getShlId()))
            && (this.getTypeId() == null ? other.getTypeId() == null : this.getTypeId().equals(other.getTypeId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getProvinceId() == null) ? 0 : getProvinceId().hashCode());
        result = prime * result + ((getCityId() == null) ? 0 : getCityId().hashCode());
        result = prime * result + ((getCountyId() == null) ? 0 : getCountyId().hashCode());
        result = prime * result + ((getProvinceName() == null) ? 0 : getProvinceName().hashCode());
        result = prime * result + ((getCityName() == null) ? 0 : getCityName().hashCode());
        result = prime * result + ((getCountyName() == null) ? 0 : getCountyName().hashCode());
        result = prime * result + ((getMapx() == null) ? 0 : getMapx().hashCode());
        result = prime * result + ((getMapy() == null) ? 0 : getMapy().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getIsDel() == null) ? 0 : getIsDel().hashCode());
        result = prime * result + ((getIsDis() == null) ? 0 : getIsDis().hashCode());
        result = prime * result + ((getDelTime() == null) ? 0 : getDelTime().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getImg() == null) ? 0 : getImg().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getLinkman() == null) ? 0 : getLinkman().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        result = prime * result + ((getServices() == null) ? 0 : getServices().hashCode());
        result = prime * result + ((getTimes() == null) ? 0 : getTimes().hashCode());
        result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
        result = prime * result + ((getTypes() == null) ? 0 : getTypes().hashCode());
        result = prime * result + ((getSysUserId() == null) ? 0 : getSysUserId().hashCode());
        result = prime * result + ((getAgentId() == null) ? 0 : getAgentId().hashCode());
        result = prime * result + ((getQq() == null) ? 0 : getQq().hashCode());
        result = prime * result + ((getWebsite() == null) ? 0 : getWebsite().hashCode());
        result = prime * result + ((getCates() == null) ? 0 : getCates().hashCode());
        result = prime * result + ((getIsTop() == null) ? 0 : getIsTop().hashCode());
        result = prime * result + ((getCatesSecond() == null) ? 0 : getCatesSecond().hashCode());
        result = prime * result + ((getTargetId() == null) ? 0 : getTargetId().hashCode());
        result = prime * result + ((getIsRight() == null) ? 0 : getIsRight().hashCode());
        result = prime * result + ((getWebsiteIsHide() == null) ? 0 : getWebsiteIsHide().hashCode());
        result = prime * result + ((getSwwg() == null) ? 0 : getSwwg().hashCode());
        result = prime * result + ((getFwzl() == null) ? 0 : getFwzl().hashCode());
        result = prime * result + ((getSpzl() == null) ? 0 : getSpzl().hashCode());
        result = prime * result + ((getPsxl() == null) ? 0 : getPsxl().hashCode());
        result = prime * result + ((getCreateDate() == null) ? 0 : getCreateDate().hashCode());
        result = prime * result + ((getCreateBy() == null) ? 0 : getCreateBy().hashCode());
        result = prime * result + ((getOrderSort() == null) ? 0 : getOrderSort().hashCode());
        result = prime * result + ((getTel() == null) ? 0 : getTel().hashCode());
        result = prime * result + ((getPartnerId() == null) ? 0 : getPartnerId().hashCode());
        result = prime * result + ((getSort() == null) ? 0 : getSort().hashCode());
        result = prime * result + ((getIsAdv() == null) ? 0 : getIsAdv().hashCode());
        result = prime * result + ((getIsAppo() == null) ? 0 : getIsAppo().hashCode());
        result = prime * result + ((getSmsMobile() == null) ? 0 : getSmsMobile().hashCode());
        result = prime * result + ((getFenleiId() == null) ? 0 : getFenleiId().hashCode());
        result = prime * result + ((getIsAdmin() == null) ? 0 : getIsAdmin().hashCode());
        result = prime * result + ((getFollowNum() == null) ? 0 : getFollowNum().hashCode());
        result = prime * result + ((getSourceId() == null) ? 0 : getSourceId().hashCode());
        result = prime * result + ((getDadaShopId() == null) ? 0 : getDadaShopId().hashCode());
        result = prime * result + ((getIsSynchro() == null) ? 0 : getIsSynchro().hashCode());
        result = prime * result + ((getIsDada() == null) ? 0 : getIsDada().hashCode());
        result = prime * result + ((getDadaCityName() == null) ? 0 : getDadaCityName().hashCode());
        result = prime * result + ((getDadaPwd() == null) ? 0 : getDadaPwd().hashCode());
        result = prime * result + ((getIsYingye() == null) ? 0 : getIsYingye().hashCode());
        result = prime * result + ((getIsTakeoutTop() == null) ? 0 : getIsTakeoutTop().hashCode());
        result = prime * result + ((getIsEfp() == null) ? 0 : getIsEfp().hashCode());
        result = prime * result + ((getIsPcTop() == null) ? 0 : getIsPcTop().hashCode());
        result = prime * result + ((getIsBanner() == null) ? 0 : getIsBanner().hashCode());
        result = prime * result + ((getUpdateDate() == null) ? 0 : getUpdateDate().hashCode());
        result = prime * result + ((getRefundAddress() == null) ? 0 : getRefundAddress().hashCode());
        result = prime * result + ((getRefundLinkman() == null) ? 0 : getRefundLinkman().hashCode());
        result = prime * result + ((getRefundMobile() == null) ? 0 : getRefundMobile().hashCode());
        result = prime * result + ((getShl() == null) ? 0 : getShl().hashCode());
        result = prime * result + ((getDetail() == null) ? 0 : getDetail().hashCode());
        result = prime * result + ((getSerPro() == null) ? 0 : getSerPro().hashCode());
        result = prime * result + ((getShlId() == null) ? 0 : getShlId().hashCode());
        result = prime * result + ((getTypeId() == null) ? 0 : getTypeId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", shl=").append(shl);
        sb.append(", detail=").append(detail);
        sb.append(", serPro=").append(serPro);
        sb.append(", shlId=").append(shlId);
        sb.append(", typeId=").append(typeId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
