package com.zheng.upms.dao.mapper;


import com.zheng.upms.dao.model.UpmsPartner;
import com.zheng.upms.dao.model.UpmsPartnerExample;
import com.zheng.upms.dao.model.UpmsPartnerWithBLOBs;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UpmsPartnerMapper {
    long countByExample(UpmsPartnerExample example);

    int deleteByExample(UpmsPartnerExample example);

    int deleteByPrimaryKey(String id);

    int insert(UpmsPartnerWithBLOBs record);

    int insertSelective(UpmsPartnerWithBLOBs record);

    List<UpmsPartnerWithBLOBs> selectByExampleWithBLOBs(UpmsPartnerExample example);

    List<UpmsPartner> selectByExample(UpmsPartnerExample example);

    UpmsPartnerWithBLOBs selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") UpmsPartnerWithBLOBs record, @Param("example") UpmsPartnerExample example);

    int updateByExampleWithBLOBs(@Param("record") UpmsPartnerWithBLOBs record, @Param("example") UpmsPartnerExample example);

    int updateByExample(@Param("record") UpmsPartner record, @Param("example") UpmsPartnerExample example);

    int updateByPrimaryKeySelective(UpmsPartnerWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(UpmsPartnerWithBLOBs record);

    int updateByPrimaryKey(UpmsPartner record);
}
