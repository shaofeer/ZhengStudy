package com.zheng.upms.rpc.api;

import com.zheng.common.base.BaseService;
import com.zheng.upms.dao.model.UpmsPartner;
import com.zheng.upms.dao.model.UpmsPartnerExample;

import java.util.List;

/**
 * @description: description:
 * <p>
 * @author: upuptop
 * <p>
 * @qq: 337081267
 * <p>
 * @CSDN: http://blog.csdn.net/pyfysf
 * <p>
 * @cnblogs: http://www.cnblogs.com/upuptop
 * <p>
 * @blog: http://wintp.top
 * <p>
 * @email: pyfysf@163.com
 * <p>
 * @time: 2018/12/2018/12/26
 * <p>
 */
public interface UpmsPartnerService extends BaseService<UpmsPartner, UpmsPartnerExample> {
}
