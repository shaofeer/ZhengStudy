package com.zheng.upms.rpc.api;

import com.zheng.upms.dao.model.UpmsPartner;
import com.zheng.upms.dao.model.UpmsPartnerExample;

import java.util.List;

/**
 * @description: description:
 * <p>
 * @author: upuptop
 * <p>
 * @qq: 337081267
 * <p>
 * @CSDN: http://blog.csdn.net/pyfysf
 * <p>
 * @cnblogs: http://www.cnblogs.com/upuptop
 * <p>
 * @blog: http://wintp.top
 * <p>
 * @email: pyfysf@163.com
 * <p>
 * @time: 2018/12/2018/12/26
 * <p>
 */
public class UpmsPartnerServiceMock implements UpmsPartnerService{

    @Override
    public int countByExample(UpmsPartnerExample example) {
        return 0;
    }

    @Override
    public int deleteByExample(UpmsPartnerExample example) {
        return 0;
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(UpmsPartner upmsPartner) {
        return 0;
    }

    @Override
    public int insertSelective(UpmsPartner upmsPartner) {
        return 0;
    }

    @Override
    public List<UpmsPartner> selectByExampleWithBLOBs(UpmsPartnerExample example) {
        return null;
    }

    @Override
    public List<UpmsPartner> selectByExample(UpmsPartnerExample example) {
        return null;
    }

    @Override
    public List<UpmsPartner> selectByExampleWithBLOBsForStartPage(UpmsPartnerExample example, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public List<UpmsPartner> selectByExampleForStartPage(UpmsPartnerExample example, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public List<UpmsPartner> selectByExampleWithBLOBsForOffsetPage(UpmsPartnerExample example, Integer offset, Integer limit) {
        return null;
    }

    @Override
    public List<UpmsPartner> selectByExampleForOffsetPage(UpmsPartnerExample example, Integer offset, Integer limit) {
        return null;
    }

    @Override
    public UpmsPartner selectFirstByExample(UpmsPartnerExample example) {
        return null;
    }

    @Override
    public UpmsPartner selectFirstByExampleWithBLOBs(UpmsPartnerExample example) {
        return null;
    }

    @Override
    public UpmsPartner selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public int updateByExampleSelective(UpmsPartner upmsPartner, UpmsPartnerExample example) {
        return 0;
    }

    @Override
    public int updateByExampleWithBLOBs(UpmsPartner upmsPartner, UpmsPartnerExample example) {
        return 0;
    }

    @Override
    public int updateByExample(UpmsPartner upmsPartner, UpmsPartnerExample example) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeySelective(UpmsPartner upmsPartner) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(UpmsPartner upmsPartner) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(UpmsPartner upmsPartner) {
        return 0;
    }

    @Override
    public int deleteByPrimaryKeys(String ids) {
        return 0;
    }

    @Override
    public void initMapper() {

    }

    //@Override
    //public long countByExample(UpmsPartnerExample example) {
    //    return 0;
    //}
}
